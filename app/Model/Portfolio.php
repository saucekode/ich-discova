<?php

App::uses('AppModel', 'Model');

class Portfolio extends AppModel {
  public $belongsTo = array('SubDetail');
  public $hasMany = array('PortfolioDoc');
}
?>
