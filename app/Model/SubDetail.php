<?php

App::uses('AppModel', 'Model');

class SubDetail extends AppModel {
  public $belongsTo = array('User','Category');
  public $hasMany = array('UserSocial','UserClient','UserService','Testimonial','Portfolio');
  public $hasOne = array('SubRating');
}
?>
