<?php

App::uses('AppModel', 'Model');

class SubRating extends AppModel {
  public $belongsTo = array('SubDetail');
  public $hasMany = array('UserRating');
}
?>
