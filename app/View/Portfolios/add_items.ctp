<div id="custom-content" class="white-popup-block" style="max-width:600px; margin: 20px auto;overflow:scroll;max-height:500px;">

<style>
@import "<?php echo $this->webroot;?>js/datetimepicker/css/bootstrap-datetimepicker.min.css";
</style>

<h4>Add New Items to "<?=$portfolio['Portfolio']['title'];?>" Portfolio</h4><hr>

<button class="btn btn-warning btn-sm" id="btn_add_portfolio_item" style="margin-bottom:10px;">
  Add New Item
</button>


  <div id="div_add_portfolio_item" style="display:none;">

      <div id="success_port_add"></div>

      <form name="" id="add_new_port_doc" novalidate="" action="<?=$this->webroot."portfolio_docs/ajax_add";?>" style="font-size:12px;" enctype="multipart/form-data">

          <?php echo $this->Form->hidden('PortfolioDoc.portfolio_id',array('value'=>$portfolio['Portfolio']['id']));?>

          <div class="row control-group">
              <div class="form-group col-xs-12 col-lg-6 floating-label-form-group controls">
                  <label>What type of portfolio?</label>
                  <?php echo $this->Form->input('PortfolioDoc.portfolio_type_id',array('class'=>'form-control input-sm','div'=>false,'label'=>false,'options'=>$portfolio_types,'empty'=>'--Select portfolio type--','find_url'=>$this->webroot."portfolio_types/get_form_details"));?>
              </div>
              <div class="col-xs-6" style="margin-top: 35px;left:-20px;" id="spinner"></div>
          </div>



          <div id="form_details"></div>

          <div class="row">
              <div class="form-group col-xs-12">
                  <button type="submit" class="btn btn-md btn-primary btn-square btn-sm" id="add_port_doc_save_btn" disabled="disabled">Save</button>
              </div>
          </div>
      </form>


  </div>

  <hr>

  <h6>
    List of items in "<?=$portfolio['Portfolio']['title'];?>" Portfolio: <span class="text-danger"><?= count($portfolio['PortfolioDoc']);?></span>
  </h6>

  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr style="font-size:14px !important;">
          <th>S/N</th>
          <th>Type</th>
          <th>Title</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>

        <?php if(!empty($portfolio['PortfolioDoc'])):?>
          <?php foreach($portfolio['PortfolioDoc'] as $key => $doc):?>
            <tr style="font-size:14px !important;">
              <td width="5%"><?=$key + 1;?></td>

              <td width="20%">
                <?php
                $get_port_name = $this->requestAction('/portfolio_types/get_title/'.$doc['portfolio_type_id']);
                echo $get_port_name;
                ?>
              </td>

              <td width="50%">
                <?=$doc['title'];?>
              </td>

              <td>
                <span>
                  <a class="btn btn-danger btn-xs" id="<?=$doc['id'];?>" action="<?= $this->webroot."portfolio_docs/ajax_delete"?>">
                    <i class="fa fa-remove" title="Delete"></i>
                  </a>
                </span>
              </td>
            </tr>
          <?php endforeach;?>
        <?php else:?>

          <tr style="font-size:14px !important;">
            <td colspan="4" class="text-center">No items found</td>
          </tr>

        <?php endif;?>
      </tbody>
    </table>


  </div>

  <?php $this->set('validation_for_this_page','assets/js/profile_tab.js');?>

  <?php echo $this->element('ich_footer_scripts');?>

</div>
