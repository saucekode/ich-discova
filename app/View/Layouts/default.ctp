<!DOCTYPE html>
<html lang="en">
<?php echo $this->element('ich_header');?>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<div id="wrapper">
		<?php echo $this->element('ich_menu');?>
		<?php echo $this->fetch('content');?>
		<?php echo $this->element('sql_dump'); ?>
		<?php echo $this->element('ich_footer');?>
	</div>

	<?php echo $this->element('ich_footer_scripts');?>

	<script src="<?php echo $this->webroot;?>assets/magnific-popup/jquery.magnific-popup.js"></script>

	<script type="text/javascript">

		$('.popup-gallery').each(function() {
			// the containers for all your galleries
	    $(this).magnificPopup({
	        delegate: 'a', // the selector for gallery item
	        type: 'image',
					tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading image...',
	        gallery: {
	          enabled:true
	        }
	    });
		});
	</script>

</body>
</html>
