<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title_for_layout;?> - Discover.NG</title>

    <script src="<?php echo $this->webroot;?>assets/js/jquery-1.10.2.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->webroot;?>assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="<?php echo $this->webroot;?>assets/css/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

    <!-- Retina Images Plugin -->
    <script src="<?php echo $this->webroot;?>assets/js/plugins/retina/retina.min.js"></script>

    <!-- Plugin CSS -->
    <link href="<?php echo $this->webroot;?>assets/css/plugins/hover/hover.min.css" rel="stylesheet">
    <link href="<?php echo $this->webroot;?>assets/css/plugins/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $this->webroot;?>assets/css/plugins/owl.carousel/owl.theme.css" rel="stylesheet">
    <link href="<?php echo $this->webroot;?>assets/css/plugins/owl.carousel/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo $this->webroot;?>assets/css/plugins/jquery.fs.wallpaper/jquery.fs.wallpaper.css" rel="stylesheet">
    <link href="<?php echo $this->webroot;?>assets/css/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">


    <!-- DEMO CSS AND STYLE SWITCHER - DEMO PURPOSES ONLY! -->
    <link id="changeable-colors" rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/spectrum-rust.css">
    <link href="<?php echo $this->webroot;?>assets/demo/style-switcher.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo $this->webroot;?>assets/js/html5shiv.js"></script>
      <script src="<?php echo $this->webroot;?>assets/js/respond.min.js"></script>
    <![endif]-->

    <style>
    p.help-block > ul{
      list-style: none !important;
      padding-left: 0px !important;
    }
    p.help-block > ul > li{
      font-size: 12px !important;
    }
    .error{
      font-weight: 300;
      color: red;
      font-size: 12px;
    }
    </style>

    <script src="<?php echo $this->webroot;?>starr/starrr.js"></script>

</head>
