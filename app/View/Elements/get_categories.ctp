<?php $categories = $this->requestAction('/categories/get_cat_menu');?>
<ul class="nav nav-pills nav-stacked nav-email shadow mb-20">
  <?php foreach($categories as $cat):?>
    <li>
      <a href="<?=$this->webroot."category-users/".$cat['Category']['name'];?>">
        <?=$cat['Category']['name'];?>
      </a>
    </li>
  <?php endforeach;?>
</ul>
