<?php $active_user = $this->Session->read('Auth.User');?>

<?php $users = $this->requestAction('/sub_details/get_similar_profiles/'.$user[0]['SubDetail']['category_id'] .DS. $user[0]['User']['id']);?>

<?php if(!empty($users)):?>

    <div class="row">
      <div class="col-sm-12 col-md-12">
        <h4>Similar Profiles</h4>

        <div style="border-top:1px solid #eee;margin-bottom:10px;"></div>

        <?php foreach($users as $user):?>
          <?php
          if(empty($user['SubDetail']['profile_pic'])){
            $profile_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
          } else {
            $profile_img = $this->webroot."img/profiles/".$user['SubDetail']['profile_pic'];
          }
          ?>

          <div class="media">
            <a class="pull-left" href="<?=$this->webroot."profile/".$user['SubDetail']['discover_url'];?>">
              <img class="media-object" src="<?=$profile_img;?>" style="width: 50px;">
            </a>
            <div class="media-body">
                <h5 class="media-heading text-danger" style="font-size: 12px;">
                  <a href="<?=$this->webroot."profile/".$user['SubDetail']['discover_url'];?>">
                    <?=$user['User']['name'];?>
                  </a>
                </h5>

                <?php if(!empty($user['User']['about_me'])):?>
                  <p style="font-size: 11px;">
                    <?=$this->Text->truncate($user['User']['about_me'],50,array('ellipsis' => '...','exact' => true));?>
                  </p>
                <?php endif;?>

            </div>
        </div>
        <?php endforeach;?>

        <?php if(empty($active_user)):?>
          <a class="btn btn-primary btn-xs" href="#login-modal" data-toggle="modal">Create your profile</a>
        <?php endif;?>

        <div style="border-top:1px solid #eee;margin-top:10px;"></div>
      </div>
    </div>

  <?php endif;?>

    <div class="row">
      <div class="col-sm-12 col-md-12">
        <h4>Adverts</h4>
        <div style="border-top:1px solid #eee;margin-bottom:10px;"></div>

        <img class="img-responsive" src="<?php echo $this->webroot."img/adv.jpeg"?>"/>
      </div>
    </div>
