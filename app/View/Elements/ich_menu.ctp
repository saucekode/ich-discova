<?php $active_user = $this->Session->read('Auth.User');?>
<nav class="navbar navbar-dark navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="<?php echo $this->webroot;?>">
              <span class="logo">
                  <!--img src="assets/img/logo.png" alt=""-->
                  <h2 class="script" style="text-transform: capitalize;margin-top: 0;">Discova.ng</h2>
              </span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="<?=$this->webroot."all-users";?>">Active Users</a>
                </li>
                <?php if(!empty($active_user)):?>
                  <li class="page-scroll">
                      <a href="<?php echo $this->webroot."dashboard"?>">Manage your Portfolio</a>
                  </li>
                  <li class="page-scroll">
                      <a href="<?php echo $this->webroot."logout"?>">Logout</a>
                  </li>
                <?php else:?>
                  <li class="page-scroll">
                      <a href="#login-modal" data-toggle="modal">Get Started</a>
                  </li>
                  <li class="page-scroll">
                      <a href="#">Contact</a>
                  </li>
                <?php endif;?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
