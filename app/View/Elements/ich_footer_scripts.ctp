<!-- Core JavaScript Files -->
<script src="<?php echo $this->webroot;?>assets/js/jquery-1.10.2.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/modernizr.custom.js"></script>

<!-- Plugin JavaScript Files -->
<script src="<?php echo $this->webroot;?>assets/js/plugins/jquery.easing/jquery.easing.1.3.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/jquery.fitvids/jquery.fitvids.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/jquery.fs.wallpaper/jquery.fs.wallpaper.min.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/scrollReveal/scrollReveal.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/stellar/stellar.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/SmoothScroll/SmoothScroll.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/jqBootstrapValidation/jqBootstrapValidation.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/plugins/isotope/isotope.pkgd.min.js"></script>



<!-- Spectrum JavaScript Files -->
<script src="<?php echo $this->webroot;?>assets/js/spectrum.nav.js"></script>
<script src="<?php echo $this->webroot;?>assets/js/spectrum.js"></script>

<?php echo $this->element('ich_get_started');?>

<?php echo $this->element('validationscript');?>


<?php if(isset($validation_for_this_page)):?>
  <script src="<?php echo $this->webroot.$validation_for_this_page;?>"></script>
<?php endif;?>

<script src="<?php echo $this->webroot;?>assets/js/get_started_form.js"></script>
