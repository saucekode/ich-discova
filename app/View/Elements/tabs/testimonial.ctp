<button class="btn btn-warning btn-sm" id="btn_toggle_add_testimonial" style="margin-bottom:20px;">Add a new testimonial</button>

<div id="div_add_testimonial" style="display:none;">

    <div id="success_testimonial"></div>

    <form name="testimonial_add" id="testimonial_add" novalidate="" action="<?=$this->webroot."testimonials/ajax_add";?>">

        <?php echo $this->Form->hidden('SubDetail.id');?>

        <div class="row control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Who gave you this testimonial?</label>
              <?php echo $this->Form->input('Testimonial.name',array('class'=>'form-control','div'=>false,'label'=>false));?>
          </div>
        </div>

        <div class="row control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Where does he/she work?</label>
              <?php echo $this->Form->input('Testimonial.person_details',array('class'=>'form-control','div'=>false,'label'=>false));?>
          </div>
        </div>

        <div class="row control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>What did he/she say about you?</label>
              <?php echo $this->Form->textarea('Testimonial.description',array('class'=>'form-control','div'=>false,'label'=>false));?>
          </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="add_testimonial_save_btn">Save</button>
            </div>
        </div>
    </form>

<hr>

</div>


<h4>List of your submitted testimonials (<span class="text-danger"><?= count($user[0]['SubDetail']['Testimonial']);?></span>)</h4>

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr style="font-size:14px !important;">
        <th>S/N</th>
        <th>Name</th>
        <th>Person Details</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php if(!empty($user[0]['SubDetail']['Testimonial'])):?>
        <?php foreach($user[0]['SubDetail']['Testimonial'] as $key => $testimonial):?>
          <tr style="font-size:14px !important;">
            <td width="5%"><?=$key + 1;?></td>
            <td width="20%">
              <?=$testimonial['name'];?>
            </td>
            <td width="25%">
              <?=$testimonial['person_details'];?>
            </td>
            <td width="40%">
              <?=$testimonial['description'];?>
            </td>
            <td>
              <span>
                <a class="btn btn-info btn-xs edit-client-link" href="<?php echo $this->webroot."user_clients/ajax_edit_client/".$testimonial['id'];?>">
                  <i class="fa fa-pencil" title="Edit"></i>
                </a>
              </span>

              <span>
                <a class="btn btn-danger btn-xs client-delete" id="<?=$testimonial['id'];?>" action="<?= $this->webroot."user_clients/ajax_client_delete"?>">
                  <i class="fa fa-remove" title="Delete"></i>
                </a>
              </span>
            </td>
          </tr>
        <?php endforeach;?>
      <?php else:?>

        <tr style="font-size:14px !important;">
          <td colspan="5" class="text-center">No social networks found</td>
        </tr>

      <?php endif;?>
    </tbody>
  </table>

  <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
  <script src="<?php APP. DS;?>assets/magnific-popup/jquery.magnific-popup.js"></script>
  <script type="text/javascript">
    $('.edit-client-link').magnificPopup({
      type: 'ajax',
      tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading client...',
    });
  </script>

</div>
