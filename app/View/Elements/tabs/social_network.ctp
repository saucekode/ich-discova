<?php
$social_networks = $this->requestAction('/social_networks/getSocialNetworksList');
?>

<button class="btn btn-warning btn-sm" id="btn_toggle_add_social_network" style="margin-bottom:20px;">Add a new social network</button>

<div id="div_add_social_network" style="display:none;">

    <div id="success_social_network"></div>

    <form name="social_network_add" id="social_network_add" novalidate="" action="<?=$this->webroot."user_socials/ajax_add";?>">

        <?php echo $this->Form->hidden('SubDetail.id');?>

        <div class="row control-group">
            <div class="form-group col-xs-6 floating-label-form-group controls">
                <label>Social Network</label>
                <?php echo $this->Form->input('UserSocial.social_network_id',array('class'=>'form-control','div'=>false,'label'=>false,'options'=>$social_networks,'empty'=>'--Select network--','find_url'=>$this->webroot."social_networks/getSocialBaseUrl"));?>
            </div>

            <div class="form-group col-xs-6 floating-label-form-group controls">
              <label>URL</label>
              <div class="input-group">
                <span class="input-group-addon base_url"></span>
                <?php echo $this->Form->input('UserSocial.title',array('class'=>'form-control','div'=>false,'label'=>false));?>
              </div>
              <label id="UserSocialTitle-error" class="error" for="UserSocialTitle"></label>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="add_social_save_btn">Save</button>
            </div>
        </div>
    </form>

<hr>

</div>


<h4>List of your Social Networks (<span class="text-danger"><?= count($user[0]['SubDetail']['UserSocial']);?></span>)</h4>

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr style="font-size:14px !important;">
        <th>S/N</th>
        <th>Social Network</th>
        <th>Profile URL</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php if(!empty($user[0]['SubDetail']['UserSocial'])):?>
        <?php foreach($user[0]['SubDetail']['UserSocial'] as $key => $social):?>
          <tr style="font-size:14px !important;">
            <td width="5%"><?=$key + 1;?></td>
            <td width="15%" class="text-center">
              <i class="fa fa-<?=$social['SocialNetwork']['font_code'];?>"></i>
            </td>
            <td width="65%">
              <a href="<?=$social['SocialNetwork']['base_url'].$social['title'];?>">
                <?php
                $out = preg_replace('/(?<=^.{22}).{4,}(?=.{20}$)/', '...', $social['SocialNetwork']['base_url'].$social['title']);
                echo $out, "\n";
                ?>
              </a>
            </td>
            <td>
              <span>
                <a class="btn btn-info btn-xs edit-client-link" href="<?php echo $this->webroot."user_clients/ajax_edit_client/".$social['id'];?>">
                  <i class="fa fa-pencil" title="Edit"></i>
                </a>
              </span>

              <span>
                <a class="btn btn-danger btn-xs client-delete" id="<?=$social['id'];?>" action="<?= $this->webroot."user_clients/ajax_client_delete"?>">
                  <i class="fa fa-remove" title="Delete"></i>
                </a>
              </span>
            </td>
          </tr>
        <?php endforeach;?>
      <?php else:?>

        <tr style="font-size:14px !important;">
          <td colspan="5" class="text-center">No social networks found</td>
        </tr>

      <?php endif;?>
    </tbody>
  </table>

  <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
  <script src="<?php APP. DS;?>assets/magnific-popup/jquery.magnific-popup.js"></script>
  <script type="text/javascript">
    $('.edit-client-link').magnificPopup({
      type: 'ajax',
      tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading client...',
    });
  </script>

</div>
