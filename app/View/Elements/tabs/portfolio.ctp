<button class="btn btn-warning btn-sm" id="btn_toggle_add_portfolio" style="margin-bottom:20px;">Add a new portfolio</button>

<div id="div_add_portfolio" style="display:none;">

    <div id="success_portfolio"></div>

    <form name="portfolio_add" id="portfolio_add" novalidate="" action="<?=$this->webroot."portfolios/ajax_add";?>">

        <?php echo $this->Form->hidden('SubDetail.id');?>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Portfolio Title</label>
                <?php echo $this->Form->input('Portfolio.title',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'e.g. Websites, Prints, Music, Videos'));?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="add_portfolio_save_btn">Save</button>
            </div>
        </div>
    </form>

</div>

<hr>

<h4>List of your portfolios (<span class="text-danger"><?= count($user[0]['SubDetail']['Portfolio']);?></span>)</h4>

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr style="font-size:14px !important;">
        <th>S/N</th>
        <th>Title</th>
        <th>No. of items</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php if(!empty($user[0]['SubDetail']['Portfolio'])):?>
        <?php foreach($user[0]['SubDetail']['Portfolio'] as $key => $portfolio):?>
          <tr style="font-size:14px !important;">
            <td width="10%"><?=$key + 1;?></td>
            <td width="45%">
              <?=$portfolio['title'];?>
            </td>
            <td width="20%">
              <?=count($portfolio['PortfolioDoc']);?>
            </td>
            <td>
              <span>
                <a class="btn btn-xs btn-success add-items-link" href="<?php echo $this->webroot."portfolios/add_items/".$portfolio['id'];?>">
                  <i class="fa fa-plus" title="Add Items"></i> Add Items
                </a>
              </span>

              <span>
                <a class="btn btn-info btn-xs edit-portfolio-link" href="<?php echo $this->webroot."portfolios/ajax_edit/".$portfolio['id'];?>">
                  <i class="fa fa-pencil" title="Edit"></i>
                </a>
              </span>

              <span>
                <a class="btn btn-danger btn-xs portfolio-delete" id="<?=$portfolio['id'];?>" action="<?= $this->webroot."portfolios/ajax_delete"?>">
                  <i class="fa fa-remove" title="Delete"></i>
                </a>
              </span>
            </td>
          </tr>
        <?php endforeach;?>
      <?php else:?>

        <tr style="font-size:14px !important;">
          <td colspan="4" class="text-center">No portfolio found</td>
        </tr>

      <?php endif;?>
    </tbody>
  </table>

  <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
  <script src="<?php APP. DS;?>assets/magnific-popup/jquery.magnific-popup.js"></script>
  <script type="text/javascript">
    $('.edit-portfolio-link').magnificPopup({
      type: 'ajax',
      tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading portfolio...',
    });
    $('.add-items-link').magnificPopup({
      type: 'ajax',
      tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading Form...',
    });
  </script>

</div>
