<style>
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
}
</style>


<div id="success"></div>

<div class="row">
  <div class="col-md-8">

    <form name="profile_update" id="profile_update" novalidate="" action="<?=$this->webroot."users/ajax_update_profile";?>">

        <?php echo $this->Form->hidden('User.id');?>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Name</label>
                <?php echo $this->Form->input('User.name',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Phone Number</label>
                <?php echo $this->Form->input('User.phone',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>About Me</label>
                <?php echo $this->Form->textarea('User.about_me',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="profile_save_btn">Save</button>
            </div>
        </div>
    </form>

  </div>

  <div class="col-md-4">
    <div class="form-group">
      <h5>* Change your profile picture</h5>

      <div class="main-img-preview">
        <?php
        if(!empty($user[0]['SubDetail']['profile_pic'])){
          $user_img = $this->webroot."img/profiles/".$user[0]['SubDetail']['profile_pic'];
        } else {
          $user_img = $this->webroot."img/avatar.jpg";
        }
        ?>

        <img class="thumbnail img-preview" src="<?=$user_img;?>" title="Preview Logo">
      </div>
      <div class="input-group">
        <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none;">
        <div class="input-group-btn">
          <div class="fileUpload btn btn-danger fake-shadow">
            <span id="profile_uploader"><i class="glyphicon glyphicon-upload"></i> Upload Picture</span>
            <input id="logo-id" name="logo" type="file" class="attachment_upload">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>





<script>
$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);

        var data = new FormData();
        jQuery.each(jQuery('#logo-id')[0].files, function(i, file) {
          data.append('file-'+i, file);
        });

        jQuery.ajax({
          url: '<?php echo $this->webroot."sub_details/upload_profile_pix"?>',
          data: data,
          cache: false,
          contentType: false,
          processData: false,
          type: 'POST',
          beforeSend: function(){
            $("#profile_uploader").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Uploading...');
          },
          success: function(data){
            if(data == 'success'){
              alert("Profile successfully updated");
              location.reload();
            } else {
              alert("Profile could not be updated");
            }
          }
        });

    });
});
</script>
