<button class="btn btn-warning btn-sm" id="btn_toggle_add_client" style="margin-bottom:20px;">Add a new client</button>

<div id="div_add_client" style="display:none;">

    <div id="success_client"></div>

    <form name="client_add" id="client_add" novalidate="" action="<?=$this->webroot."user_clients/ajax_add_client";?>">

        <?php echo $this->Form->hidden('SubDetail.id');?>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Client Name</label>
                <?php echo $this->Form->input('UserClient.name',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Logo URL</label>
                <?php echo $this->Form->input('UserClient.logo_url',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Website</label>
                <?php echo $this->Form->input('UserClient.website',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="add_client_save_btn">Save</button>
            </div>
        </div>
    </form>


</div>

<hr>

<h4>List of your clients (<span class="text-danger"><?= count($user[0]['SubDetail']['UserClient']);?></span>)</h4>

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr style="font-size:14px !important;">
        <th>S/N</th>
        <th>Logo</th>
        <th>Client Name</th>
        <th>Website</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php if(!empty($user[0]['SubDetail']['UserClient'])):?>
        <?php foreach($user[0]['SubDetail']['UserClient'] as $key => $client):?>
          <tr style="font-size:14px !important;">
            <td width="5%"><?=$key + 1;?></td>
            <td width="20%">
              <img style="height:20px" class="img-responsive" src="<?=$client['logo_url'];?>"/>
            </td>
            <td width="35%">
              <?=$client['name'];?>
            </td>
            <td width="25%">
              <a href="<?=$client['website'];?>">
                <?php
                $out = preg_replace('/(?<=^.{22}).{4,}(?=.{20}$)/', '...', $client['website']);
                echo $out, "\n";
                ?>
              </a>
            </td>
            <td>
              <span>
                <a class="btn btn-info btn-xs edit-client-link" href="<?php echo $this->webroot."user_clients/ajax_edit_client/".$client['id'];?>">
                  <i class="fa fa-pencil" title="Edit"></i>
                </a>
              </span>

              <span>
                <a class="btn btn-danger btn-xs client-delete" id="<?=$client['id'];?>" action="<?= $this->webroot."user_clients/ajax_client_delete"?>">
                  <i class="fa fa-remove" title="Delete"></i>
                </a>
              </span>
            </td>
          </tr>
        <?php endforeach;?>
      <?php else:?>

        <tr style="font-size:14px !important;">
          <td colspan="5" class="text-center">No clients found</td>
        </tr>

      <?php endif;?>
    </tbody>
  </table>

  <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
  <script src="<?php APP. DS;?>assets/magnific-popup/jquery.magnific-popup.js"></script>
  <script type="text/javascript">
    $('.edit-client-link').magnificPopup({
      type: 'ajax',
      tLoading: '<span class="fa fa-spinner fa-spin"></span> Loading client...',
    });
  </script>

</div>
