<style>
/* USER PROFILE PAGE */
.card {
   margin-top: 20px;
   padding: 30px;
   background-color: rgba(214, 224, 226, 0.2);
   -webkit-border-top-left-radius:5px;
   -moz-border-top-left-radius:5px;
   border-top-left-radius:5px;
   -webkit-border-top-right-radius:5px;
   -moz-border-top-right-radius:5px;
   border-top-right-radius:5px;
   -webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
   box-sizing: border-box;
}
.card.hovercard {
   position: relative;
   padding-top: 0;
   overflow: hidden;
   text-align: center;
   background-color: #fff;
   background-color: rgba(255, 255, 255, 1);
}
.card.hovercard .card-background {
   height: 130px;
}
.card-background img {
   -webkit-filter: blur(25px);
   -moz-filter: blur(25px);
   -o-filter: blur(25px);
   -ms-filter: blur(25px);
   filter: blur(25px);
   margin-left: -100px;
   margin-top: -200px;
   min-width: 130%;
}
.card.hovercard .useravatar {
   position: absolute;
   top: 15px;
   left: 0;
   right: 0;
}
.card.hovercard .useravatar img {
   width: 100px;
   height: 100px;
   max-width: 100px;
   max-height: 100px;
   -webkit-border-radius: 50%;
   -moz-border-radius: 50%;
   border-radius: 50%;
   border: 5px solid rgba(255, 255, 255, 0.5);
}
.card.hovercard .card-info {
   position: absolute;
   bottom: 14px;
   left: 0;
   right: 0;
}
.card.hovercard .card-info .card-title {
   padding:0 5px;
   font-size: 20px;
   line-height: 1;
   color: #262626;
   background-color: rgba(255, 255, 255, 0.1);
   -webkit-border-radius: 4px;
   -moz-border-radius: 4px;
   border-radius: 4px;
}
.card.hovercard .card-info {
   overflow: hidden;
   font-size: 12px;
   line-height: 20px;
   color: #737373;
   text-overflow: ellipsis;
}
.card.hovercard .bottom {
   padding: 0 20px;
   margin-bottom: 17px;
}
.btn-pref .btn {
   -webkit-border-radius:0 !important;
}
.tab_font_12{
 font-size: 12px;
}
.truncate{
 overflow: hidden;
 white-space: nowrap;
 text-overflow: ellipsis;
}

.white-popup-block {
   background: #FFF;
   padding: 20px 30px;
   text-align: left;
   margin: 40px auto;
   position: relative
}


/* resume stuff */

.bs-callout {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #eee;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 1px 5px;
    margin-bottom: 5px;
    padding: 20px;
}
.bs-callout:last-child {
    margin-bottom: 0px;
}
.bs-callout h4 {
    margin-bottom: 10px;
    margin-top: 0;
}

.bs-callout-danger {
    border-left-color: #d9534f;
}

.bs-callout-danger h4{
    color: #d9534f;
    font-size: 18px
}

.bs-callout-danger p{
    margin: 0 0 10px
    line-height: 20px;
    margin-bottom: 10px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    font-size: 14px;
}

.resume .list-group-item:first-child, .resume .list-group-item:last-child{
  border-radius:0;
}

.panel-teal .panel-heading {
    background-color: #37BC9B;
    border: 1px solid #36b898;
    color: white;
}

.panel .panel-heading {
    padding: 5px;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    border-bottom: 1px solid #DDD;
    -moz-border-radius: 0px;
    -webkit-border-radius: 0px;
    border-radius: 0px;
}

.panel .panel-heading .panel-title {
    padding: 10px;
    font-size: 17px;
}
.list-group-item>.list-group-item-heading{
  color: #333;
  font-size:16px;
}
.font14{
  font-size:14px !important;
  margin-bottom: 10px;
}

/* ========================================================================
 * MAIL
 * ======================================================================== */
.nav-email > li:first-child + li:active {
  margin-top: 0px;
}
.nav-email > li + li {
  margin-top: 1px;
}
.nav-email li {
  background-color: white;
}
.nav-email li.active {
  background-color: transparent;
}
.nav-email li.active .label {
  background-color: white;
  color: black;
}
.nav-email li a {
  color: black;
  -moz-border-radius: 0px;
  -webkit-border-radius: 0px;
  border-radius: 0px;
}
.nav-email li a:hover {
  background-color: #EEEEEE;
}
.nav-email li a i {
  margin-right: 5px;
}
.nav-email li a .label {
  margin-top: -1px;
}

.table-email tr:first-child td {
  border-top: none;
}
.table-email tr td {
  vertical-align: top !important;
}
.table-email tr td:first-child, .table-email tr td:nth-child(2) {
  text-align: center;
  width: 35px;
}
.table-email tr.unread, .table-email tr.selected {
  background-color: #EEEEEE;
}
.table-email .media {
  margin: 0px;
  padding: 0px;
  position: relative;
}
.table-email .media h4 {
  margin: 0px;
  font-size: 14px;
  line-height: normal;
}
.table-email .media-object {
  width: 35px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}
.table-email .media-meta, .table-email .media-attach {
  font-size: 11px;
  color: #999;
  position: absolute;
  right: 10px;
}
.table-email .media-meta {
  top: 0px;
}
.table-email .media-attach {
  bottom: 0px;
}
.table-email .media-attach i {
  margin-right: 10px;
}
.table-email .media-attach i:last-child {
  margin-right: 0px;
}
.table-email .email-summary {
  margin: 0px 110px 0px 0px;
}
.table-email .email-summary strong {
  color: #333;
}
.table-email .email-summary span {
  line-height: 1;
}
.table-email .email-summary span.label {
  padding: 1px 5px 2px;
}
.table-email .ckbox {
  line-height: 0px;
  margin-left: 8px;
}
.table-email .star {
  margin-left: 6px;
}
.table-email .star.star-checked i {
  color: goldenrod;
}

.nav-email-subtitle {
  font-size: 15px;
  text-transform: uppercase;
  color: #333;
  margin-bottom: 15px;
  margin-top: 30px;
}


.src-image {
  display: none;
}

.card2 {
  overflow: hidden;
  position: relative;
  border: 1px solid #CCC;
  border-radius: 8px;
  text-align: center;
  padding: 0;
  /*background-color: #284c79;*/
  color: rgb(136, 172, 217);
}

.card2 .header-bg {
  /* This stretches the canvas across the entire hero unit */
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 70px;
  border-bottom: 1px #FFF solid;
  /* This positions the canvas under the text */
  z-index: 1;
}
.card2 .avatar {
  position: relative;
  margin-top: 15px;
  z-index: 100;
}

.card2 .avatar img {
  width: 100px;
  height: 100px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 5px solid rgba(0,0,30,0.8);
}

</style>
