<footer class="footer-1">
  <div class="upper">
      <div class="container">
          <div class="row">
              <div class="col-md-3">
                  <h2 class="script">Discova.ng</h2>
                  <address>
                      <strong>Intelligent Campaign Hub</strong><br>1, Kola Adeyina Close<br>Lekki Phase 1, Lagos, Nigeria<br>
                      <abbr title="Phone">P: </abbr>(234) 803-7890-345
                  </address>
              </div>
              <div class="col-md-3">
                  <h4>Features</h4>
                  <ul class="list-unstyled footer-links">
                      <li>
                          <a href="#">Blog</a>
                      </li>
                      <li>
                          <a href="#">How it works</a>
                      </li>
                      <li>
                          <a href="#">Sample Portfolios</a>
                      </li>
                  </ul>
              </div>
              <div class="col-md-3">
                  <h4>Useful Links</h4>
                  <ul class="list-unstyled footer-links">
                      <li>
                          <a href="#">Terms of use</a>
                      </li>
                      <li>
                          <a href="#">Privacy Policy</a>
                      </li>
                      <li>
                          <a href="#">Refund Policy</a>
                      </li>
                  </ul>
              </div>
              <div class="col-md-3">
                  <h4>Social</h4>
                  <ul class="list-inline">
                      <li>
                          <a href="#" class="btn btn-social-light btn-facebook"><i class="fa fa-fw fa-facebook"></i></a>
                      </li>
                      <li>
                          <a href="#" class="btn btn-social-light btn-twitter"><i class="fa fa-fw fa-twitter"></i></a>
                      </li>
                      <li>
                          <a href="#" class="btn btn-social-light btn-dribbble"><i class="fa fa-fw fa-dribbble"></i></a>
                      </li>
                      <li>
                          <a href="#" class="btn btn-social-light btn-google-plus"><i class="fa fa-fw fa-google-plus"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
  <div class="lower">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <span class="small">Copyright © 2016 - Discover.ng. All Rights Reserved.</span>
              </div>
          </div>
      </div>
  </div>
</footer>
