<?php $active_user = $this->Session->read('Auth.User');?>
<?php if(empty($active_user)):?>

<div id="login-modal" class="portfolio-modal modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content" style="background-color: rgba(0,0,0,0.5);">
      <div class="close-modal" data-dismiss="modal">
				<div class="lr" style="background-color: #fff !important;">
						<div class="rl" style="background-color: #fff !important;">
						</div>
				</div>
      </div>
      <div class="container" style="padding:70px;">

          <div class="row">
              <div class="col-md-8 col-md-offset-2 col-md-onset-2">

								<div class="login-form signup-form" style="padding:30px;background:#fff;">
									<h4 class="text-center">START YOUR FREE TRIAL</h4>
									<div class="text-center">
										<small>
                      14 days free. No Credit Card Required
                    </small>
										<hr class="light" style="border-color: #d86044;">

									</div>


									<form class="" id="getStartedForm" novalidate="" method="POST" action="<?php echo $this->webroot."users/ajax_sign_up"?>">
	                  <div class="row control-group">
	                      <div class="form-group col-xs-12 floating-label-form-group controls">
	                          <label>Name</label>
	                          <input type="text" class="form-control" placeholder="Enter your full name" name="get_started_name" required="" data-validation-required-message="Please enter your name.">
	                      </div>
	                  </div>
	                  <div class="row control-group">
	                      <div class="form-group col-lg-6 col-xs-12 floating-label-form-group controls">
	                          <label>Email Address</label>
	                          <input type="email" class="form-control" placeholder="Your valid e-mail address" name="get_started_email" required="" data-validation-required-message="Please enter your email address.">
	                      </div>

                        <div class="form-group col-lg-6 col-xs-12 floating-label-form-group controls">
	                          <label>Password</label>
	                          <input type="password" class="form-control" placeholder="Your preferred password" name="get_started_password" required="" data-validation-required-message="Please enter your password." minlength="6">
	                      </div>

	                  </div>

                    <div class="row control-group">
                      <?php $categories = $this->requestAction('/categories/get_categories');?>
	                      <div class="form-group col-lg-6 col-xs-12 floating-label-form-group controls">
	                          <label>Category</label>

                            <select required="" name="get_started_category_id" id="get_started_category_id" class="form-control">
                              <?php foreach($categories as $key => $cat):?>
                                <option value="<?=$key;?>"><?=$cat;?></option>
                              <?php endforeach;?>
                            </select>
	                      </div>

                        <div class="form-group col-lg-6 col-xs-12 floating-label-form-group controls">
	                          <label>Discover.ng URL</label>
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Your Discover.NG url" name="get_started_url" required="" data-validation-required-message="Please enter your Discover.NG URL.">
                              <span class="input-group-addon">.discover.ng</span>
                            </div>
                            <label id="get_started_url-error" class="error" for="get_started_url"></label>
	                      </div>
	                  </div>


	                  <div id="success"></div>
	                  <div class="row">
	                      <div class="form-group col-xs-12 text-center">
	                          <button type="submit" id="get_started_submit" class="btn btn-md btn-primary">Get Started</button>
	                      </div>
	                  </div>
	              	</form>

                  <div class="text-center">
										<hr style="border-color: #d86044;">
										<small>Already have an account? <a href="<?=$this->webroot."login"?>">Login Now</a></small>
									</div>

								</div>

              </div>
          </div>
      </div>
  </div>
</div>

<?php endif;?>
