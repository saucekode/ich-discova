<style>
.navbar{
  background: #3e444d;
}
</style>

<section style="padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" data-scrollreveal="enter left over 1s" data-sr-init="true" data-sr-complete="true">
                <h1 style="font-size:100px;">Aww!</h1>
                <p>The profile you tried to access was not found.</p>
                <hr>

            </div>
        </div>
    </div>
</section>


<?php
$featured_profiles = $this->requestAction('/sub_details/get_featured_users');

function split_name($name, $prefix='')
{
  $pos = strrpos($name, ' ');

  if ($pos === false) {
    return array(
     $prefix . 'firstname' => $name,
     $prefix . 'surname' => null
    );
  }

  $firstname = substr($name, 0, $pos + 1);
  $surname = substr($name, $pos);

  return array(
    $prefix . 'firstname' => $firstname,
    $prefix . 'surname' => $surname
  );
}
?>

<?php if(!empty($featured_profiles)):?>
<!-- About Option 1 -->
<section class="about-1" style="padding-top:0px;">
    <div class="container">
        <div class="row" data-scrollreveal="move 0 over 1s">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h3>Here are some of our top most "DISCOVA"d profiles</h3>
            </div>
        </div>
        <div class="row" data-scrollreveal="enter bottom over 1s">
            <div class="col-lg-12">


                <div id="about-1-carousel" class="text-center">

                  <?php foreach($featured_profiles as $profile):?>

      <style>
      .about-img-<?=$profile['SubDetail']['id'];?>{
        <?php
        if(!empty($profile['SubDetail']['profile_pic'])){
          $user_img = $this->webroot."img/profiles/".$profile['SubDetail']['profile_pic'];
        } else {
          $user_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
        }
        ?>
        background-image: url("<?=$user_img;?>");
      }
      </style>


<a href="<?=$this->webroot."profile/".$profile['SubDetail']['discover_url'];?>">
                  <div class="item about-img-<?=$profile['SubDetail']['id'];?>">
                      <div class="info">
                          <!-- Mobile Fallback Image -->
                          <img class="img-responsive img-circle visible-xs" src="<?=$user_img;?>" alt="">

                          <?php $this_user = split_name($profile['User']['name']);?>
                          <!-- Name / Position / Social Links -->
                          <h3><?=$this_user['firstname'];?><br><?=$this_user['surname'];?></h3>

                          <?php if(!empty($profile['UserSocial'])):?>
                            <ul class="list-inline">
                              <?php foreach($profile['UserSocial'] as $social):?>
                                <li>
                                  <a class="light-text" href="<?=$social['SocialNetwork']['base_url'].$social['title'];?>">
                                    <i class="fa fa-<?=$social['SocialNetwork']['font_code'];?> fa-fw"></i>
                                  </a>
                                </li>
                              <?php endforeach;?>
                            </ul>
                          <?php endif;?>

                      </div>
                  </div>

                </a>

                  <?php endforeach;?>


                </div>




            </div>
        </div>
    </div>
</section>

<?php endif;?>
