<?php
$dir = 'img/background_images/';
$images = glob($dir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
$randomImage = $images[array_rand($images)];
?>


<header class="intro-img intro-dark-bg" style="background-image: url('<?php echo $this->webroot.$randomImage;?>')" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="intro-body" data-scrollreveal="move 0">
        <div class="container">
            <div class="intro-welcome"><span class="text-primary">Get Discovered!</span></div>
            <br>
            <h1 class="brand-heading">Create Your Online Portfolio</h1>
						<br>
						<a class="btn btn-default btn-lg" href="#login-modal" data-toggle="modal">Get Started Now</a>
            <hr class="light">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <h4>No matter what you do...</h4>
                    </div>
                    <div class="item">
                        <h4>No matter where you live...</h4>
                    </div>
										<div class="item">
                        <h4>You need to get discovered</h4>
                    </div>
                </div>
            </div>
            <div class="page-scroll" data-scrollreveal="enter bottom after .6s">
                <a href="#about" class="btn btn-scroll-light sink">
                    <i class="fa fa-angle-double-down"></i>
                </a>
            </div>
        </div>
    </div>
</header>



<?php
$featured_profiles = $this->requestAction('/sub_details/get_featured_users');

function split_name($name, $prefix='')
{
  $pos = strrpos($name, ' ');

  if ($pos === false) {
    return array(
     $prefix . 'firstname' => $name,
     $prefix . 'surname' => null
    );
  }

  $firstname = substr($name, 0, $pos + 1);
  $surname = substr($name, $pos);

  return array(
    $prefix . 'firstname' => $firstname,
    $prefix . 'surname' => $surname
  );
}
?>

<?php if(!empty($featured_profiles)):?>
<!-- About Option 1 -->
<section id="about" class="about-1">
    <div class="container">
        <div class="row" data-scrollreveal="move 0 over 1s">
            <div class="col-lg-12 text-center">
                <h2>Featured Profiles</h2>
                <hr class="primary">
            </div>
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <p>Here are some of our top most "DISCOVA"d profiles</p>
            </div>
        </div>
        <div class="row" data-scrollreveal="enter bottom over 1s">
            <div class="col-lg-12">


                <div id="about-1-carousel" class="text-center">

                  <?php foreach($featured_profiles as $profile):?>

      <style>
      .about-img-<?=$profile['SubDetail']['id'];?>{
        <?php
        if(!empty($profile['SubDetail']['profile_pic'])){
          $user_img = $this->webroot."img/profiles/".$profile['SubDetail']['profile_pic'];
        } else {
          $user_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
        }
        ?>
        background-image: url("<?=$user_img;?>");
      }
      </style>


<a href="<?=$this->webroot."profile/".$profile['SubDetail']['discover_url'];?>">
                  <div class="item about-img-<?=$profile['SubDetail']['id'];?>">
                      <div class="info">
                          <!-- Mobile Fallback Image -->
                          <img class="img-responsive img-circle visible-xs" src="<?=$user_img;?>" alt="">

                          <?php $this_user = split_name($profile['User']['name']);?>
                          <!-- Name / Position / Social Links -->
                          <h3><?=$this_user['firstname'];?><br><?=$this_user['surname'];?></h3>

                          <?php if(!empty($profile['UserSocial'])):?>
                            <ul class="list-inline">
                              <?php foreach($profile['UserSocial'] as $social):?>
                                <li>
                                  <a class="light-text" href="<?=$social['SocialNetwork']['base_url'].$social['title'];?>">
                                    <i class="fa fa-<?=$social['SocialNetwork']['font_code'];?> fa-fw"></i>
                                  </a>
                                </li>
                              <?php endforeach;?>
                            </ul>
                          <?php endif;?>

                      </div>
                  </div>

                </a>

                  <?php endforeach;?>


                </div>




            </div>
        </div>
    </div>
</section>

<?php endif;?>








<!--section id="portfolio" class="portfolio-1 bg-lighter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" data-scrollreveal="move 0">
                <h2>Portfolio</h2>
                <hr class="primary">
                <ul class="list-inline" id="filters">
                    <li>
                        <button data-filter="*" type="button" class="btn btn-primary btn-square btn-raised">All</button>
                    </li>
                    <li>
                        <button data-filter=".web" type="button" class="btn btn-primary btn-square btn-raised">Website</button>
                    </li>
                    <li>
                        <button data-filter=".graphic" type="button" class="btn btn-primary btn-square btn-raised">Graphic</button>
                    </li>
                    <li>
                        <button data-filter=".print" type="button" class="btn btn-primary btn-square btn-raised">Print</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row" data-scrollreveal="enter bottom">
            <div class="isotope">
                <div class="col-sm-4 portfolio-item web">
                    <a href="#portfolio-modal-1" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/1.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item graphic">
                    <a href="#portfolio-modal-2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/2.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item print">
                    <a href="#portfolio-modal-1" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/3.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item web">
                    <a href="#portfolio-modal-2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/4.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item graphic">
                    <a href="#portfolio-modal-1" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/5.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item print">
                    <a href="#portfolio-modal-2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/6.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item web">
                    <a href="#portfolio-modal-2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/7.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item graphic">
                    <a href="#portfolio-modal-1" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/8.jpg" class="img-centered" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item print">
                    <a href="#portfolio-modal-2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <h3><i class="fa fa-search fa-2x"></i>
                                </h3>
                            </div>
                        </div>
                        <img src="assets/img/demo-portfolio/9.jpg" class="img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section-->

<!-- Standard Portfolio Modal - Video and Text -->
<!--div id="portfolio-modal-2" class="portfolio-modal modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
          <div class="lr">
              <div class="rl">
              </div>
          </div>
      </div>
      <div class="container">
          <div class="row first">
              <div class="col-lg-12">
                  <h2 class="page-header">Project Name</h2>
              </div>
          </div>
          <div class="row">
              <div class="col-md-8">
                  <iframe src="http://player.vimeo.com/video/87701971?title=0&byline=0&portrait=0" width="400" height="225" style="border: none;"></iframe>
              </div>
              <div class="col-md-4">
                  <h3>Project Description:</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, et, ea, accusantium reprehenderit aliquam numquam fuga natus fugiat odit distinctio illo at unde.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, iusto molestiae possimus sint dignissimos! Laudantium, dolore, vel, sint, labore optio perferendis illo dolorum similique soluta eum cupiditate assumenda consequatur maiores.</p>
              </div>
          </div>
          <div class="row">
              <div class="col-md-8">
                  <h3>Project Details:</h3>
                  <ul class="list-unstyled project-details">
                      <li>
                          <strong>Client:</strong>Lorem Ipsum</li>
                      <li>
                          <strong>Services:</strong>Web Design, Photography</li>
                      <li>
                          <strong>Date:</strong>January 2015</li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div-->
