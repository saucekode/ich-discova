<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<title>Welcome to Discova.com.ng</title>
</head>
<body>

<style type="text/css">

	#outlook a{padding:0;}
	body{width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}
	.ExternalClass{width:100%;}
	.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
	.bodytbl{margin:0;padding:0;width:100% !important;}
	img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%;}
	a img{border:none;}
	p{margin:1em 0;}

	table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}
	table td{border-collapse:collapse;}
	.o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}

	body{background-color:#F3F4F4/*Background Color*/;}
	table{font-family:Helvetica,Arial,sans-serif;font-size:12px;color:#585858;}
	td,p{line-height:24px;color:#585858/*Text*/;}
	td,tr{padding:0;}
	ul,ol{margin-top:24px;margin-bottom:24px;}
	li{line-height:24px;}

	a{color:#5ca8cd/*Contrast*/;text-decoration:none;padding:2px 0px;}
	a:link{color:#5ca8cd;}
	a:visited{color:#5ca8cd;}
	a:hover{color:#5ca8cd;}

	.h1{font-family:Helvetica,Arial,sans-serif;font-size:26px;letter-spacing:-1px;margin-bottom:16px;margin-top:2px;line-height:30px;}
	.h2{font-family:Helvetica,Arial,sans-serif;font-size:20px;letter-spacing:0;margin-top:2px;line-height:30px;}
	h1,h2,h3,h4,h5,h6{font-family:Helvetica,Arial,sans-serif;font-weight:normal;}
	h1{font-size:20px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;line-height:24px;}
	h2{font-size:18px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h3{font-size:14px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h4{font-size:14px;font-weight:bold;}
	h5{font-size:12px;}
	h6{font-size:12px;font-weight:bold;}
	h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#5ca8cd;}
	h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#5ca8cd !important;}
	h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#5ca8cd !important;}

	.wrap.header{border-top:1px solid #FEFEFE/*Content Border*/;}
	.wrap.footer{border-bottom:1px solid #FEFEFE;}
	.wrap.body,.wrap.header,.wrap.footer{background-color:#FFFFFF/*Body Background*/;border-right:1px solid #FEFEFE;border-left:1px solid #FEFEFE;}
	.padd{width:24px;}

	.small{font-size:11px;line-height:18px;}
	.separator{border-top:1px dotted #E1E1E1/*Separator Line*/;}
	.btn{margin-top:10px;display:block;}
	.btn img{display:inline;}
	.subline{line-height:18px;font-size:16px;letter-spacing:-1px;}

	table.textbutton td{background:#efefef/*Text Button Background*/;padding:1px 14px 4px 14px;color:#585858;display:block;height:22px;border:1px solid #FEFEFE;vertical-align:top;}
	table.textbutton a{color:#585858;font-size:13px;font-weight:normal;line-height:22px;width:100%;display:inline-block;}

	div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;display:none;visibility:hidden;}

	@media only screen and (max-device-width: 480px) {
		body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}
		table[class=bodytbl] .subline{float:left;}
		table[class=bodytbl] .padd{width:12px !important;}
		table[class=bodytbl] .wrap{width:470px !important;}
		table[class=bodytbl] .wrap table{width:100% !important;}
		table[class=bodytbl] .wrap img{max-width:100% !important;height:auto !important;}
		table[class=bodytbl] .wrap .m-100{width:100% !important;}
		table[class=bodytbl] .m-0{width:0;display:none;}
		table[class=bodytbl] .m-b{display:block;width:100% !important;}
		table[class=bodytbl] .m-b-b{margin-bottom:24px !important;}
		table[class=bodytbl] .m-1-2{max-width:264px !important;}
		table[class=bodytbl] .m-1-3{max-width:168px !important;}
		table[class=bodytbl] .m-1-4{max-width:120px !important;}
		table[class=bodytbl] .m-1-2 img{max-width:264px !important;}
		table[class=bodytbl] .m-1-3 img{max-width:168px !important;}
		table[class=bodytbl] .m-1-4 img{max-width:120px !important;}
	}

	@media only screen and (max-device-width: 320px) {
		table[class=bodytbl] .wrap{width:310px !important;}
	}

</style>
<table class="bodytbl" width="100%" cellspacing="0" cellpadding="0">
<tbody><tr>
	<td background="" align="center">

		<table width="600" cellspacing="0" cellpadding="0" class="wrap">
		<tbody><tr>
			<td height="24" align="center" valign="middle">
				<div class="small">
          <single label="preintro" style="font-size:20px;font-family: 'Pacifico', 'Helvetica Neue',Helvetica, Arial, sans-serif;">Discova.com.ng</single>
          <a name="top"></a>
        </div>
			</td>
		</tr>
		</tbody></table>




<modules>











	<module label="1/2 Floating Image Left" auto="">
		<table width="600" cellspacing="0" cellpadding="0" class="wrap body">
		<tbody><tr><td height="12"></td></tr>
		<tr>
			<td valign="top" align="center">
				<table width="100%" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td width="24" class="padd">&nbsp;</td>

					<td valign="top" align="left">


						<multi label="Body">
              <p>Dear <?php echo $user['User']['name'];?>,</p>
              <p>Thank you for signing up to Discova.com.ng. To get started, you will need to verify your email address. Please click the link below to verify your account:</p>
              <p>
                <?php
                $url = Router::url('/',true)."users/activate_account/".$user['User']['confirmation_code'];
                echo $url;
                ?>
              </p>
              <p>Best Regards,<br>Discova.com.ng</p>
            </multi>

					</td>

					<td width="24" class="padd">&nbsp;</td>
				</tr>
				</tbody></table>
			</td>
		</tr>
		<tr><td height="12"></td></tr>
		</tbody></table>
	</module>


		<table width="600" cellspacing="0" cellpadding="0" class="wrap body">
			<tbody><tr><td height="12" colspan="3"></td></tr>
			<tr><td width="24" class="padd">&nbsp;</td>
			<td align="center"><table width="100%" cellpadding="0" cellspacing="0" class="separator"><tbody><tr><td height="23">&nbsp;</td></tr></tbody></table></td>
			<td width="24" class="padd">&nbsp;</td>
			</tr>
		</tbody></table>
























































</modules>
		<table width="600" cellspacing="0" cellpadding="0" class="wrap footer">
		<tbody><tr>
			<td width="24" class="padd">&nbsp;</td>
			<td valign="top" align="center">
				<table width="100%" cellpadding="0" cellspacing="0" class="o-fix">
				<tbody><tr>

					<td valign="top" align="left">
						<table width="528" cellpadding="0" cellspacing="0" align="left">
						<tbody><tr>
							<td class="small m-b" align="left" valign="top">
								<div><single label="CAN-SPAM">1, Kola Adeyina Close, Lekki Phase1, Lagos</single></div>
							</td>
						</tr>
						</tbody></table>

					</td>

				</tr>
				</tbody></table>
			</td>
			<td width="24" class="padd">&nbsp;</td>
		</tr>
		<tr><td height="24" colspan="3"></td></tr>
		</tbody></table>




	</td>
</tr>
</tbody></table>

</body></html>
