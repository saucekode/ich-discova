<style>
.navbar{
  background: #3e444d;
}
@import "<?php echo $this->webroot;?>magnific-popup/magnific-popup.css";
</style>
<?php echo $this->element('user_card_stylesheet');?>

<section>
  <div class="container">
      <div class="row">

        <div class="col-lg-3 col-sm-3">
          <div class="card hovercard">
              <div class="card-background">
                  <img class="card-bkimg" alt="" src="<?php echo $this->webroot."assets/img/profile_bg.jpeg"?>">
              </div>
              <div class="useravatar">
                <?php
                if(!empty($user[0]['SubDetail']['profile_pic'])){
                  $user_img = $this->webroot."img/profiles/".$user[0]['SubDetail']['profile_pic'];
                } else {
                  $user_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
                }
                ?>

                  <img alt="" src="<?php echo $user_img;?>">
              </div>
              <br>
              <div class="card-info">
                <span class="card-title">
                  <?=$user[0]['User']['name'];?>
                </span>
                <br>
                <small><cite title="Lagos, Nigeria">Lagos, Nigeria <i class="fa fa-map-marker">
                </i></cite></small>
              </div>
          </div>

          <div class="well">
            <div class="tab-content">

              <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p style="font-size:14px !important;margin-bottom:5px;">
                      <i class="fa fa-envelope"></i> <?=$user[0]['User']['email'];?>

                      <br />
                      <i class="fa fa-globe"></i> <a href="http://<?=$user[0]['SubDetail']['discover_url'];?>.discova.com.ng">
                        <?=$user[0]['SubDetail']['discover_url'];?>.discova.com.ng
                      </a>

                      <?php if(!empty($user[0]['User']['phone'])):?>
                      <br />
                      <i class="fa fa-phone"></i> <?=$user[0]['User']['phone'];?>
                      <?php endif;?>

                      <br />
                      <i class="fa fa-calendar"></i>
                      <?php echo date('F d, Y',strtotime($user[0]['User']['created']));?>
                    </p>

                    <?php if(!empty($user[0]['SocialNetwork'])):?>
                    <!-- Split button -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-xs">
                            My Social Profiles</button>
                        <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown">
                            <span class="caret"></span><span class="sr-only">Social</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Google +</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Github</a></li>
                        </ul>
                    </div>
                    <br>
                    <?php endif;?>

                    <br>

                </div>
              </div>

              <p style="font-size:14px !important;">
                <?php
                if(!empty($user[0]['User']['about_me'])):
                  echo $user[0]['User']['about_me'];
                else:
                  echo "Tell us about yourself...";
                endif;
                ?>
              </p>

            </div>
          </div>
        </div>


        <div class="col-lg-9 col-sm-9">
          <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="..." style="margin-top: 20px;">
              <div class="btn-group" role="group">
                  <button type="button" id="profile" class="btn btn-primary" href="#tab1" data-toggle="tab">
                      <div class="tab_font_12">Profile</div>
                  </button>
              </div>
              <!--div class="btn-group" role="group">
                  <button type="button" id="clients" class="btn btn-default" href="#tab2" data-toggle="tab">
                      <div class="tab_font_12">Clients</div>
                  </button>
              </div-->
              <div class="btn-group" role="group">
                  <button type="button" id="portfolio" class="btn btn-default" href="#tab3" data-toggle="tab">
                      <div class="tab_font_12">Portfolio</div>
                  </button>
              </div>
              <div class="btn-group" role="group">
                  <button type="button" id="social_network" class="btn btn-default" href="#tab4" data-toggle="tab">
                      <div class="tab_font_12">Social Networks</div>
                  </button>
              </div>
              <div class="btn-group" role="group">
                  <button type="button" id="testimonial" class="btn btn-default" href="#tab5" data-toggle="tab">
                      <div class="tab_font_12">Testimonials</div>
                  </button>
              </div>
          </div>

              <div class="well">
            <div class="tab-content">


              <div class="tab-pane fade in active" id="tab1">

                <?php echo $this->element('tabs/profile');?>

              </div>


              <!--div class="tab-pane fade in" id="tab2">

                <?php echo $this->element('tabs/client');?>

              </div-->


              <div class="tab-pane fade in" id="tab3">

                <?php echo $this->element('tabs/portfolio');?>

              </div>


              <div class="tab-pane fade in" id="tab4">

                <?php echo $this->element('tabs/social_network');?>

              </div>


              <div class="tab-pane fade in" id="tab5">

                <?php echo $this->element('tabs/testimonial');?>

              </div>


            </div>
          </div>
        </div>




      </div>
  </div>
</section>


<?php $this->set('validation_for_this_page','assets/js/profile_tab.js');?>
