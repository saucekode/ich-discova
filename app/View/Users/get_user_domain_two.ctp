<?php $this->set('title_for_layout',$user[0]['User']['name']);?>

<style>
.navbar{
  background: #3e444d;
}
@import "<?php echo $this->webroot;?>magnific-popup/magnific-popup.css";
@import "<?php echo $this->webroot;?>starr/starrr.css";
</style>
<?php $active_session = $this->Session->read('Auth.User.id');?>

<?php echo $this->element('user_card_stylesheet');?>


<section>
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-sm-3">
        <div class="card hovercard">
            <div class="card-background" style="height: 170px;">
                <img class="card-bkimg" alt="" src="<?php echo $this->webroot."assets/img/profile_bg.jpeg"?>">
            </div>
            <div class="useravatar">
              <?php
              if(!empty($user[0]['SubDetail']['profile_pic'])){
                $user_img = $this->webroot."img/profiles/".$user[0]['SubDetail']['profile_pic'];
              } else {
                $user_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
              }
              ?>

                <img alt="" src="<?php echo $user_img;?>">
            </div>
            <br>
            <div class="card-info">
              <span class="card-title">
                <?=$user[0]['User']['name'];?>
              </span>
              <!--br>
              <small>
                <cite title="Lagos, Nigeria"><i class="fa fa-map-marker"></i> Lagos, Nigeria</cite>
              </small-->


              <br>
              <p class='starrr' id='user_rating' style="margin-bottom:0px;"></p><small> (<?= $user[0]['SubDetail']['SubRating']['number_votes']?> votes)</small>
              <input type='text' name='rating' value="<?= $user[0]['SubDetail']['SubRating']['whole_avg']?>" id='user_rating_input' style="display:none;width: 30px;margin: 10px 0;" />

              <script>
              var $s2input = $('#user_rating_input');
              $('#user_rating').starrr({
                max: 5,
                rating: $s2input.val(),
                change: function(e, value){
                  var current_user = '<?php echo $active_session;?>';
                  if(current_user === ''){
                    alert("You have to be logged in to rate a user.");
                    window.location.href = '<?php echo $this->webroot."login";?>';
                  } else {
                    $s2input.val(value).trigger('input');

                    var link = '<?php echo $this->webroot."user_ratings/rate_user";?>';
                    var sub_detail_id = '<?php echo $user[0]['SubDetail']['id']?>';
                    var sub_rating_id = '<?= $user[0]['SubDetail']['SubRating']['id']?>';

                    $.post(link, {'rating_value':value,'sub_detail_id':sub_detail_id,'sub_rating_id':sub_rating_id}, function(data)
                    {
                      //alert(data);
                      location.reload();
                    });
                  }

                }
              });
              </script>

            </div>
        </div>

        <div class="well">
          <div class="tab-content">

            <div class="row">
              <div class="col-sm-12 col-md-12">
                  <p style="font-size:14px !important;margin-bottom:5px;">
                    <i class="fa fa-envelope"></i> <?=$user[0]['User']['email'];?>

                    <br />
                    <i class="fa fa-globe"></i> <a href="http://<?=$user[0]['SubDetail']['discover_url'];?>.discova.com.ng" target="_blank">
                      <?=$user[0]['SubDetail']['discover_url'];?>.discova.com.ng
                    </a>

                    <?php if(!empty($user[0]['User']['phone'])):?>
                    <br />
                    <i class="fa fa-phone"></i> <?=$user[0]['User']['phone'];?>
                    <?php endif;?>

                    <br />
                    <i class="fa fa-calendar"></i>
                    <?php echo date('F d, Y',strtotime($user[0]['User']['created']));?>
                  </p>

                  <?php if(!empty($user[0]['SubDetail']['UserSocial'])):?>
                  <!-- Split button -->
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs">
                          My Social Profiles
                      </button>

                      <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown">
                          <span class="caret"></span>
                          <span class="sr-only">Social</span>
                      </button>

                      <ul class="dropdown-menu" role="menu">
                        <?php foreach($user[0]['SubDetail']['UserSocial'] as $social):?>
                          <li>
                            <a href="<?=$social['SocialNetwork']['base_url'].$social['title'];?>" target="_blank">
                              <?=$social['SocialNetwork']['name'];?>
                            </a>
                          </li>
                        <?php endforeach;?>
                      </ul>
                  </div>
                  <br>
                  <?php endif;?>

                  <br>


              </div>
            </div>

          </div>
        </div>

        <h5>Categories</h5>

        <?php echo $this->element('get_categories');?>

      </div>


      <div class="col-lg-6 col-sm-6" style="margin-top: 20px;">

        <div class="panel panel-default">

<div class="bs-callout bs-callout-danger">
  <h4>About Me</h4>
  <p>
    <?php
    if(!empty($user[0]['User']['about_me'])):
      echo $user[0]['User']['about_me'];
    else:
      echo "Tell us about yourself...";
    endif;
    ?>
  </p>
</div>

<div class="bs-callout bs-callout-danger">
<h4>My Portfolio</h4>

<?php if(!empty($user[0]['SubDetail']['Portfolio'])){?>

<div class="panel-group" id="accordion">

<?php foreach($user[0]['SubDetail']['Portfolio'] as $key => $portfolio):?>

<div class="panel panel-default">
  <div class="panel-heading" style=" background: #272b31; padding: 1px;">
    <h4 class="panel-title" style=" margin-bottom: 0px;">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$portfolio['id'];?>" style=" color: #fff;font-size:15px;">
        <?=$portfolio['title'];?></a>
      </h4>
    </div>
    <div id="collapse<?=$portfolio['id'];?>" class="panel-collapse collapse <?php if($key == 0){echo 'in';}?>">
      <div class="panel-body">

        <?php foreach($portfolio['PortfolioDoc'] as $port_doc):?>
          <div class="font14" style="border-bottom:1px solid #eee;">

            <?php
            $string = preg_replace("/[\r\n]+/", " ", $port_doc['form_details']);
            $json = utf8_encode($string);
            $decode_data = json_decode($json);
            ?>

            <p>
              <strong style="font-size:14px;"><?=$port_doc['title'];?></strong>
              <small class="pull-right label label-success"><?=$port_doc['PortfolioType']['title'];?></small>
            </p>

            <p>
              <?=$decode_data->description;?>
            </p>

            <p>

              <?php if($port_doc['PortfolioType']['title'] == 'Project'):?>
                <div class="html-code grid-of-images">
                  <div class="popup-gallery">
                    <?php foreach($decode_data->pictures as $img):?>
                      <a href="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" title="<?=$decode_data->title;?>" class="mfp-image">
                        <img src="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" style="min-height:50px;max-height:50px;max-width:50px;min-width:50px;overflow:hidden;border:2px solid #000;margin-right:5px;">
                      </a>
                    <?php endforeach;?>
                  </div>
                </div>

              <?php else:?>
                <a href="<?php echo $decode_data->url;?>" title="<?=$decode_data->title;?>" target="_blank" >
                  Click here to view
                </a>

              <?php endif;?>


            </p>
          </div>
        <?php endforeach;?>


      </div>
    </div>
  </div>
<?php endforeach;?>

</div>

<?php } else {?>

  <p class="text-center">This user hasn't added a portfolio</p>

  <?php }?>

               </div>
             </div>

      </div>



      <div class="col-lg-3 col-sm-3" style="margin-top: 20px;">
        <?php echo $this->element('user_similar_profiles');?>
      </div>


    </div>
  </div>
</section>
