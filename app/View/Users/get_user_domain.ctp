<?php $this->set('title_for_layout','User Details');?>
<?php
function split_name($name, $prefix='')
{
  $pos = strrpos($name, ' ');

  if ($pos === false) {
    return array(
     $prefix . 'firstname' => $name,
     $prefix . 'surname' => null
    );
  }

  $firstname = substr($name, 0, $pos + 1);
  $surname = substr($name, $pos);

  return array(
    $prefix . 'firstname' => $firstname,
    $prefix . 'surname' => $surname
  );
}
$users_name = split_name($user[0]['User']['name']);
 ?>
<header class="intro-img intro-dark-bg" style="background-image: url('<?php echo $this->webroot;?>assets/img/demo-bgs/demo-bg-3.jpg')" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="intro-body" data-scrollreveal="move 0">
        <div class="container">
            <div class="intro-welcome">
              <span>
                Hi, my name is
              </span>
            </div>
            <br>
            <h1 class="brand-heading"><?=$users_name['firstname'];?><br><span class="text-primary"><?=$users_name['surname'];?></span></h1>
            <hr class="light">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <h4>Welcome to my Discova.NG Profile</h4>
                    </div>
                </div>
            </div>
            <div class="page-scroll" data-scrollreveal="enter bottom after .6s">
                <a href="#about" class="btn btn-scroll-light sink">
                    <i class="fa fa-angle-double-down"></i>
                </a>
            </div>
        </div>
    </div>
</header>


<?php if(!empty($user[0]['User']['about_me'])):?>

<section id="about" class="about-2">
  <div class="container">
      <div class="row" data-scrollreveal="enter left">
          <div class="col-lg-4 col-lg-offset-1">
              <img src="<?php echo $this->webroot;?>assets/img/demo-portraits/portrait-2.jpg" class="img-circle img-responsive img-centered dark-faded-border" alt="">
          </div>
          <div class="col-lg-5 text-center" data-scrollreveal="enter right">
              <h2>About Me</h2>
              <hr class="primary">
              <p>
                <?=$user[0]['User']['about_me'];?>
              </p>



              <p>Connect with me via social media, view my
                  <span class="page-scroll"><a href="#portfolio">past work</a>
                  </span>, or
                  <span class="page-scroll"><a href="#contact">contact me</a>
                  </span>now if you're ready to get started!</p>

              <?php if(!empty($user[0]['SubDetail']['UserSocial'])):?>

                <ul class="list-inline">
                  <?php foreach($user[0]['SubDetail']['UserSocial'] as $social):?>
                    <li>
                      <a href="<?=$social['SocialNetwork']['base_url'].$social['title'];?>" class="btn btn-social-dark btn-<?=$social['SocialNetwork']['font_code'];?>" target="_blank">
                        <i class="fa fa-fw fa-<?=$social['SocialNetwork']['font_code'];?>"></i>
                      </a>
                    </li>
                  <?php endforeach;?>
                </ul>

              <?php endif;?>

          </div>
      </div>
  </div>
</section>

<?php endif;?>




<?php if(!empty($user[0]['SubDetail']['UserClient'])):?>

<section id="clients" class="clients bg-dark">
  <div class="container">
      <div class="row text-center">
          <div class="col-lg-12" data-scrollreveal="move 0">
              <h2>My Past Clients</h2>
              <hr class="light">
          </div>
          <div class="col-lg-12" data-scrollreveal="enter bottom">
              <ul id="clients-carousel" class="text-center">
                <?php foreach($user[0]['SubDetail']['UserClient'] as $client):?>
                  <li class="item">
                    <a href="<?=$client['website'];?>" target="_blank">
                      <img style="max-height:50px" class="img-responsive" src="<?=$client['logo_url'];?>" alt="">
                    </a>
                  </li>
                <?php endforeach;?>
              </ul>
          </div>
      </div>
  </div>
</section>

<?php endif;?>





<section id="contact" class="contact-1 bg-parallax-light" style="background-image: url('<?php echo $this->webroot;?>assets/img/demo-bgs/freelancer-contact.jpg');" data-stellar-background-ratio="0.5">
  <div class="container">
      <div class="row" data-scrollreveal="move 0">
          <div class="col-lg-12 text-center">
              <h2>Let's Get In Touch</h2>
              <hr class="primary">
          </div>
      </div>
      <div class="row" data-scrollreveal="enter bottom">
          <div class="col-lg-8 col-lg-offset-2">
              <form name="sentMessage" id="contactForm" novalidate>
                  <div class="row control-group">
                      <div class="form-group col-xs-12 floating-label-form-group controls">
                          <label>Name</label>
                          <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                          <p class="help-block text-danger"></p>
                      </div>
                  </div>
                  <div class="row control-group">
                      <div class="form-group col-xs-12 floating-label-form-group controls">
                          <label>Email Address</label>
                          <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                          <p class="help-block text-danger"></p>
                      </div>
                  </div>
                  <div class="row control-group">
                      <div class="form-group col-xs-12 floating-label-form-group controls">
                          <label>Message</label>
                          <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                          <p class="help-block text-danger"></p>
                      </div>
                  </div>
                  <br>
                  <div id="success"></div>
                  <div class="row">
                      <div class="form-group col-xs-12">
                          <button type="submit" class="btn btn-lg btn-primary btn-square btn-raised">Send</button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
</section>
