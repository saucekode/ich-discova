<style>
.navbar{
  background: #3e444d;
}
</style>

<section id="login" class="portfolio-1 bg-lighter" style="padding-bottom:20px;">
<div class="container">
<div class="row">
<div class="col-lg-12">

<div>
<div class="modal-content">
<div class="container" style="padding:50px;">

  <div class="row">
      <div class="col-lg-6 col-lg-offset-3 col-lg-onset-3">

        <?php echo $this->Session->flash();?>
        <div id="success"></div>

				<div class="login-form signup-form bg-dark" style="padding:30px;">
					<h4 class="text-center">LOGIN TO YOUR ACCOUNT</h4>
					<div class="text-center">
						<small>Access your account to get discovered</small>
						<hr class="light" style="border-color: #d86044;">
					</div>


					<form class="" id="loginform" novalidate="" method="POST" action="<?php echo $this->webroot."users/ajax_login"?>" redirect="<?php echo $this->webroot."dashboard"?>">
            <div class="row control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                    <label>Email Address</label>
                    <input type="email" class="form-control" placeholder="Your valid e-mail address" name="username" required="" data-validation-required-message="Please enter your email address.">
                </div>
            </div>

            <div class="row control-group">
              <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Password</label>
                  <input type="password" class="form-control" placeholder="Your chosen password" name="password" required="" data-validation-required-message="Please enter your password.">
              </div>
            </div>


            <div class="row">
                <div class="form-group col-xs-12 text-center">
                    <button type="submit" id="login_button" class="btn btn-md btn-primary">Login</button>
                </div>
            </div>
        	</form>

          <div class="text-center">
						<hr style="border-color: #d86044;">
						<small>Don't have an account? <a href="#login-modal" data-toggle="modal">Get Started</a></small>
					</div>

				</div>

      </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<?php $this->set('validation_for_this_page','assets/js/login_form.js');?>
