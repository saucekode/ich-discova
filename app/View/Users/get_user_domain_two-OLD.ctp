<style>
.navbar{
  background: #3e444d;
}
/* USER PROFILE PAGE */
.card {
   margin-top: 20px;
   padding: 30px;
   background-color: rgba(214, 224, 226, 0.2);
   -webkit-border-top-left-radius:5px;
   -moz-border-top-left-radius:5px;
   border-top-left-radius:5px;
   -webkit-border-top-right-radius:5px;
   -moz-border-top-right-radius:5px;
   border-top-right-radius:5px;
   -webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
   box-sizing: border-box;
}
.card.hovercard {
   position: relative;
   padding-top: 0;
   overflow: hidden;
   text-align: center;
   background-color: #fff;
   background-color: rgba(255, 255, 255, 1);
}
.card.hovercard .card-background {
   height: 130px;
}
.card-background img {
   -webkit-filter: blur(25px);
   -moz-filter: blur(25px);
   -o-filter: blur(25px);
   -ms-filter: blur(25px);
   filter: blur(25px);
   margin-left: -100px;
   margin-top: -200px;
   min-width: 130%;
}
.card.hovercard .useravatar {
   position: absolute;
   top: 15px;
   left: 0;
   right: 0;
}
.card.hovercard .useravatar img {
   width: 100px;
   height: 100px;
   max-width: 100px;
   max-height: 100px;
   -webkit-border-radius: 50%;
   -moz-border-radius: 50%;
   border-radius: 50%;
   border: 5px solid rgba(255, 255, 255, 0.5);
}
.card.hovercard .card-info {
   position: absolute;
   bottom: 14px;
   left: 0;
   right: 0;
}
.card.hovercard .card-info .card-title {
   padding:0 5px;
   font-size: 20px;
   line-height: 1;
   color: #262626;
   background-color: rgba(255, 255, 255, 0.1);
   -webkit-border-radius: 4px;
   -moz-border-radius: 4px;
   border-radius: 4px;
}
.card.hovercard .card-info {
   overflow: hidden;
   font-size: 12px;
   line-height: 20px;
   color: #737373;
   text-overflow: ellipsis;
}
.card.hovercard .bottom {
   padding: 0 20px;
   margin-bottom: 17px;
}
.btn-pref .btn {
   -webkit-border-radius:0 !important;
}
.tab_font_12{
 font-size: 12px;
}
.truncate{
 overflow: hidden;
 white-space: nowrap;
 text-overflow: ellipsis;
}

.white-popup-block {
   background: #FFF;
   padding: 20px 30px;
   text-align: left;
   margin: 40px auto;
   position: relative
}


/* resume stuff */

.bs-callout {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #eee;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 1px 5px;
    margin-bottom: 5px;
    padding: 20px;
}
.bs-callout:last-child {
    margin-bottom: 0px;
}
.bs-callout h4 {
    margin-bottom: 10px;
    margin-top: 0;
}

.bs-callout-danger {
    border-left-color: #d9534f;
}

.bs-callout-danger h4{
    color: #d9534f;
    font-size: 18px
}

.bs-callout-danger p{
    margin: 0 0 10px
    line-height: 20px;
    margin-bottom: 10px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    font-size: 14px;
}

.resume .list-group-item:first-child, .resume .list-group-item:last-child{
  border-radius:0;
}

.panel-teal .panel-heading {
    background-color: #37BC9B;
    border: 1px solid #36b898;
    color: white;
}

.panel .panel-heading {
    padding: 5px;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    border-bottom: 1px solid #DDD;
    -moz-border-radius: 0px;
    -webkit-border-radius: 0px;
    border-radius: 0px;
}

.panel .panel-heading .panel-title {
    padding: 10px;
    font-size: 17px;
}
.list-group-item>.list-group-item-heading{
  color: #333;
  font-size:16px;
}
.font14{
  font-size:14px !important;
  margin-bottom: 10px;
}


@import "<?php echo $this->webroot;?>magnific-popup/magnific-popup.css";
</style>



<section>
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-sm-3">
        <div class="card hovercard">
            <div class="card-background">
                <img class="card-bkimg" alt="" src="<?php echo $this->webroot."assets/img/profile_bg.jpeg"?>">
            </div>
            <div class="useravatar">
              <?php
              if(!empty($user[0]['SubDetail']['profile_pic'])){
                $user_img = $this->webroot."img/profiles/".$user[0]['SubDetail']['profile_pic'];
              } else {
                $user_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
              }
              ?>

                <img alt="" src="<?php echo $user_img;?>">
            </div>
            <br>
            <div class="card-info">
              <span class="card-title">
                <?=$user[0]['User']['name'];?>
              </span>
              <br>
              <small><cite title="Lagos, Nigeria">Lagos, Nigeria <i class="fa fa-map-marker">
              </i></cite></small>
            </div>
        </div>

        <div class="well">
          <div class="tab-content">

            <div class="row">
              <div class="col-sm-12 col-md-12">
                  <p style="font-size:14px !important;margin-bottom:5px;">
                    <i class="fa fa-envelope"></i> <?=$user[0]['User']['email'];?>

                    <br />
                    <i class="fa fa-globe"></i> <a href="http://<?=$user[0]['SubDetail']['discover_url'];?>.discova.com" target="_blank">
                      <?=$user[0]['SubDetail']['discover_url'];?>.discova.com
                    </a>

                    <?php if(!empty($user[0]['User']['phone'])):?>
                    <br />
                    <i class="fa fa-phone"></i> <?=$user[0]['User']['phone'];?>
                    <?php endif;?>

                    <br />
                    <i class="fa fa-calendar"></i>
                    <?php echo date('F d, Y',strtotime($user[0]['User']['created']));?>
                  </p>

                  <?php if(!empty($user[0]['SubDetail']['UserSocial'])):?>
                  <!-- Split button -->
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs">
                          My Social Profiles</button>
                      <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown">
                          <span class="caret"></span><span class="sr-only">Social</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Twitter</a></li>
                          <li><a href="#">Google +</a></li>
                          <li><a href="#">Facebook</a></li>
                          <li><a href="#">Github</a></li>
                      </ul>
                  </div>
                  <br>
                  <?php endif;?>

                  <br>

              </div>
            </div>

          </div>
        </div>
      </div>


      <div class="col-lg-6 col-sm-6" style="margin-top: 20px;">

        <div class="panel panel-default">

               <div class="bs-callout bs-callout-danger">
                  <h4>About Me</h4>
                  <p>
                    <?php
                    if(!empty($user[0]['User']['about_me'])):
                      echo $user[0]['User']['about_me'];
                    else:
                      echo "Tell us about yourself...";
                    endif;
                    ?>
                  </p>
               </div>

               <div class="bs-callout bs-callout-danger">
                  <h4>My Portfolio</h4>
                  <ul class="list-group">
                    <?php if(!empty($user[0]['SubDetail']['Portfolio'])):?>
                      <?php foreach($user[0]['SubDetail']['Portfolio'] as $key => $portfolio):?>

                      <div class="list-group-item">
                        <div class="backg" style="background: #000;padding-top: 10px;padding-bottom: 1px;padding-left: 10px;">
                           <h4 class="list-group-item-heading" style="margin-bottom: 10px;font-size: 17px;color: #fff;">
                             <i class="fa fa-list"></i> <?=$portfolio['title'];?>
                           </h4>
                        </div>

                         <p class="list-group-item-text">



                           <?php foreach($portfolio['PortfolioDoc'] as $port_doc):?>
                             <div class="font14" style="margin-bottom:40px;">

                               <?php $decode_data = json_decode($port_doc['form_details']);?>

                              <p>
                                <strong style="font-size:16px;"><?=$port_doc['title'];?></strong>
                                <small class="pull-right"><i class="fa fa-tags"></i></small>
                              </p>

                              <p>
                                <?=$decode_data->description;?>
                              </p>

                              <p>

                                <?php if($port_doc['PortfolioType']['title'] == 'Project'):?>
                                  <div class="html-code grid-of-images">
                                   <div class="popup-gallery">
                                     <?php foreach($decode_data->pictures as $img):?>
                                       <a href="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" title="<?=$decode_data->title;?>" class="mfp-image">
                                         <img src="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" style="min-height:50px;max-height:50px;max-width:50px;min-width:50px;overflow:hidden;border:2px solid #000;">
                                       </a>
                                     <?php endforeach;?>
                                   </div>
                                 </div>

                               <?php else:?>
                                 <a href="<?php echo $decode_data->url;?>" title="<?=$decode_data->title;?>" target="_blank" class="btn btn-success btn-xs">
                                   Download Now
                                 </a>

                               <?php endif;?>


                               </p>
                             </div>
                           <?php endforeach;?>




                         </p>
                      </div>

                    <?php endforeach;?>

                    <?php else:?>
                      <p class="text-center">No Portfolio found</p>
                    <?php endif;?>

                  </ul>


                  <div class="panel-group" id="accordion">

                    <?php foreach($user[0]['SubDetail']['Portfolio'] as $key => $portfolio):?>
  <div class="panel panel-default">
    <div class="panel-heading" style=" background: #000; padding: 1px;">
      <h4 class="panel-title" style=" margin-bottom: 0px;">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$portfolio['id'];?>" style=" color: #fff">
        <?=$portfolio['title'];?></a>
      </h4>
    </div>
    <div id="collapse<?=$portfolio['id'];?>" class="panel-collapse collapse <?php if($key == 0){echo 'in';}?>">
      <div class="panel-body">

        <?php foreach($portfolio['PortfolioDoc'] as $port_doc):?>
          <div class="font14" style="border-bottom:1px solid #eee;">

            <?php $decode_data = json_decode($port_doc['form_details']);?>

           <p>
             <strong style="font-size:14px;"><?=$port_doc['title'];?></strong>
             <small class="pull-right label label-success"><?=$port_doc['PortfolioType']['title'];?></small>
           </p>

           <p>
             <?=$decode_data->description;?>
           </p>

           <p>

             <?php if($port_doc['PortfolioType']['title'] == 'Project'):?>
               <div class="html-code grid-of-images">
                <div class="popup-gallery">
                  <?php foreach($decode_data->pictures as $img):?>
                    <a href="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" title="<?=$decode_data->title;?>" class="mfp-image">
                      <img src="<?php echo $this->webroot."img/project_pictures/".$portfolio['id'].DS.$img;?>" style="min-height:50px;max-height:50px;max-width:50px;min-width:50px;overflow:hidden;border:2px solid #000;">
                    </a>
                  <?php endforeach;?>
                </div>
              </div>

            <?php else:?>
              <a href="<?php echo $decode_data->url;?>" title="<?=$decode_data->title;?>" target="_blank" class="btn btn-success btn-xs">
                Download Now
              </a>

            <?php endif;?>


            </p>
          </div>
        <?php endforeach;?>


    </div>
    </div>
  </div>
<?php endforeach;?>

</div>

               </div>
             </div>

      </div>



      <div class="col-lg-3 col-sm-3">
      </div>


    </div>
  </div>
</section>
