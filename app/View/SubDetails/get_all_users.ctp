<style>
.navbar{
  background: #3e444d;
}
@import "<?php echo $this->webroot;?>magnific-popup/magnific-popup.css";
</style>
<?php echo $this->element('user_card_stylesheet');?>

<section>
  <div class="container">
    <div class="row">

      <div class="col-lg-2 col-sm-2" style="margin-top: 20px;">
        <h4 style="margin-top:0px;">Categories</h4>

        <?php echo $this->element('get_categories');?>
      </div>


      <div class="col-lg-7 col-sm-7" style="margin-top: 20px;">

        <h4 style="margin-top:0px;">
          All Users
        </h4>

        <div style="border-top:1px solid #eee;margin-bottom:10px;"></div>

        <?php if(!empty($users)):?>

          <div class="row">

          <?php foreach($users as $user):?>

            <?php
            if(empty($user['SubDetail']['profile_pic'])){
              $profile_img = $this->webroot."assets/img/demo-portraits/portrait-2.jpg";
            } else {
              $profile_img = $this->webroot."img/profiles/".$user['SubDetail']['profile_pic'];
            }
            ?>


            <div class="col-lg-4 col-md-6 col-sm-12">
              <div class="card2" style="border-bottom: 5px solid red;margin-bottom: 10px;">
                  <div class="avatar">
                    <img src="<?php echo $profile_img;?>" alt="">
                  </div>

                  <div class="content" style="background-color: #284c79;">
                      <p class="font14" style="margin-top: 5px;color: #fff;padding-top: 10px;">
                        <?=$user['User']['name'];?>
                        <br>
                         <small class="truncate" style="color: rgb(136, 172, 217);font-size: 11px;">

                           <?php
                           if(!empty($user['User']['about_me'])){
                             echo $this->Text->truncate($user['User']['about_me'],30,array('ellipsis' => '...','exact' => true));
                           } else {
                             echo '...';
                           }
                           ?>
                         </small>
                       </p>
                      <p style="margin-bottom: 0px;">
                        <a href="<?=$this->webroot."profile/".$user['SubDetail']['discover_url'];?>" class="btn btn-default btn-xs" style="margin-bottom: 10px;text-transform:capitalize;">
                          View Profile
                        </a>
                      </p>
                  </div>
              </div>
          </div>

          <?php endforeach;?>

        </div>

        <?php if (($this->Paginator->param('count')) >= ($this->Paginator->param('limit'))):?>
          <div class="row" style="margin-top:20px;border-top:1px solid #eee;">
            <div class="col-sm-12 text-center">
              <ul class="pagination">
                <li><?php echo $this->Paginator->prev('←' . __(''), array(), null, array('class' => 'prev disabled'));?></li>
                <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );?>
                <li><?php echo $this->Paginator->next(__('') . '→', array(), null, array('class' => 'next disabled'));?></li>
              </ul>

            </div>
          </div>
        <?php endif;?>

      <?php else:?>

        <p class="text-center">No users found in this category</p>

      <?php endif;?>

      </div>



      <div class="col-lg-3 col-sm-3" style="margin-top: 20px;">
        <?php echo $this->element('category_similar_profiles');?>
      </div>


    </div>
  </div>
</section>
