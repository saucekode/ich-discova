<div id="custom-content" class="white-popup-block" style="max-width:600px; margin: 20px auto;">

<script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
  <div>

    <h4>Edit "<?=$client['UserClient']['name'];?>"</h4><hr>

    <div id="success"></div>

    <form name="edit_client" id="edit_client" novalidate="" action="<?=$this->webroot."user_clients/submit_ajax_edit_client/";?>">

        <?php echo $this->Form->hidden('UserClient.id');?>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Client Name</label>
                <?php echo $this->Form->input('UserClient.name',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Logo URL</label>
                <?php echo $this->Form->input('UserClient.logo_url',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>
        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Website</label>
                <?php echo $this->Form->input('UserClient.website',array('class'=>'form-control','div'=>false,'label'=>false));?>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-md btn-primary btn-square btn-raised" id="edit_client_save_btn">Save</button>
            </div>
        </div>
    </form>
  </div>

<?php $this->set('validation_for_this_page','assets/js/profile_tab.js');?>

<?php echo $this->element('ich_footer_scripts');?>

</div>
