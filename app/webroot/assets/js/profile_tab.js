//This is for the tab on the user's dashboard. Basically used to change colour of active tab
$(document).ready(function() {
  $(".btn-pref .btn").click(function(){
      $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
      // $(".tab").addClass("active"); // instead of this do the below
      $(this).removeClass("btn-default").addClass("btn-primary");
  });
});

//Toggle the add a new client
$( "#btn_toggle_add_client" ).click(function() {
  $( "#div_add_client" ).slideToggle( "slow" );
});

//Toggle the add a new testimonial
$( "#btn_toggle_add_testimonial" ).click(function() {
  $( "#div_add_testimonial" ).slideToggle( "slow" );
});

//Toggle the add a new portfolio item
$( "#btn_add_portfolio_item" ).click(function() {
  $( "#div_add_portfolio_item" ).slideToggle( "slow" );
});

//Toggle the add a new portfolio item
$( "#btn_toggle_add_social_network" ).click(function() {
  $( "#div_add_social_network" ).slideToggle( "slow" );
});

//Validate and Save New Portfolio doc
$(function(){

  $("#add_new_port_doc").validate({
    rules: {
        url: {
          required:true,
          url: true
        },
        title: {
          required:true,
          maxlength: 200
        }
    },
    submitHandler: submitForm
  });
  function submitForm(form)
  {
   var formData = new FormData(form);

   $.ajax({
       url: $("#add_new_port_doc").attr("action"),
       type: "POST",
       data: formData,
       contentType: false,
       cache: false,
       processData: false,
       beforeSend: function()
       {
         $("#add_port_doc_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
       },
       success: function(data) {
         if(data == 'success'){
           // Success message
           $('#success_port_add').html("<div class='alert alert-success'>");
           $('#success_port_add > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
               .append("</button>");
           $('#success_port_add > .alert-success')
               .append("<strong>Portfolio was successfully created</strong>");
           $('#success_port_add > .alert-success')
               .append('</div>');

           //clear all fields
           $("#add_port_doc_save_btn").html('Save');
           //location.reload();
         }

         else {
           // Fail message
           $('#success_port_add').html("<div class='alert alert-danger'>");
           $('#success_port_add > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
               .append("</button>");
           $('#success_port_add > .alert-danger').append("<strong>Protfolio coun</strong>");
           $('#success_port_add > .alert-danger').append('</div>');
           //clear all fields
           //$('#getStartedForm').trigger("reset");
           $("#add_port_doc_save_btn").html('Save');
         }
       },
   });
     return false;
 }
});

//Prefill the form for the selected portfolio type
$( "#PortfolioDocPortfolioTypeId" ).change(function() {
  $("#spinner").html("<i class='fa fa-spin fa-spinner'></i>");
  var selected_val = $( "#PortfolioDocPortfolioTypeId option:selected" ).val();
  if(selected_val !== ""){
    $.ajax({
      method: "GET",
      data: {'type_id':selected_val},
      url: $(this).attr("find_url")
    }).done(function( msg ) {
      $("#form_details").html(msg);
      $("#spinner").html("");
      $("#add_port_doc_save_btn").removeAttr("disabled");
    });
  } else {
    $("#form_details").html("");
    $("#spinner").html("");
    $("#add_port_doc_save_btn").attr("disabled","disabled");
  }
});


//Prefill the form for the selected portfolio type
$( "#UserSocialSocialNetworkId" ).change(function() {
  var selected_val = $( "#UserSocialSocialNetworkId option:selected" ).val();
  if(selected_val !== ""){
    $.ajax({
      method: "GET",
      data: {'type_id':selected_val},
      url: $(this).attr("find_url")
    }).done(function( msg ) {
      $(".base_url").html(msg);
    });
  } else {
    $(".base_url").html("");
  }
});

//AJAX delete a client
$(".client-delete").click(function() {
  var id = $(this).attr("id");
  var link = $(this).attr("action");
  var del_id = id;
  var parent = $(this).parent("span").parent("td").parent("tr");
  if(confirm('Are you sure you want to delete this client?'))
  {
    $.post(link, {'del_id':del_id}, function(data)
    {
      parent.fadeOut('slow');
    });
  }
  return false;
});

//Toggle the add a new portfolio
$( "#btn_toggle_add_portfolio" ).click(function() {
  $( "#div_add_portfolio" ).slideToggle( "slow" );
});

//This is for validating a phone number
 jQuery.validator.addMethod("lettersonly", function(value, element) {
   return this.optional(element) || /^[a-z]+$/i.test(value);
 }, "Letters only please");

 jQuery.validator.addMethod("noSpace", function(value, element) {
   return value.indexOf(" ") < 0 && value != "";
 }, "Empty space is not allowed");

 jQuery.validator.addMethod("alphanumeric", function(value, element) {
   return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
 }, "Only letters and numbers are allowed");

 jQuery.validator.addMethod("timeyi", function(value, element) {
   return this.optional(element) || /^(1[012]|0[1-9]):[0-5][0-9](\\s)? (am|pm)+$/i.test(value);
 }, "Invalid Time");


//Validate and save profile update form
$(function(){
  $('#profile_update').validate({
    rules: {
        "data[User][name]": {
            required:true
        },
        "data[User][phone]":{
            maxlength: 20,
            minlength: 10,
            noSpace:true,
            number:true
        },
        "data[User][about_me]": {
            required:true
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#profile_update").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#profile_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Updating...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success').html("<div class='alert alert-success'>");
             $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-success')
                 .append("<strong>Update was successful</strong>");
             $('#success > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#profile_save_btn").html('Save');
             location.reload();
           }

           else {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Profile could not be updated. Try again.</strong>");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#profile_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});



//Validate and add a new client
$(function(){
  $('#client_add').validate({
    rules: {
        "data[UserClient][name]": {
            required:true
        },
        "data[UserClient][logo_url]":{
            required:true,
            url: true
        },
        "data[UserClient][website]": {
            required:true,
            url: true
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#client_add").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#add_client_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success_client').html("<div class='alert alert-success'>");
             $('#success_client > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_client > .alert-success')
                 .append("<strong>Update was successful</strong>");
             $('#success_client > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#add_client_save_btn").html('Save');
             location.reload();
           }

           else {
             // Fail message
             $('#success_client').html("<div class='alert alert-danger'>");
             $('#success_client > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_client > .alert-danger').append("<strong>Profile could not be updated. Try again.</strong>");
             $('#success_client > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#add_client_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});


//Validate and edit a new client
$(function(){
  $('#edit_client').validate({
    rules: {
        "data[UserClient][name]": {
            required:true
        },
        "data[UserClient][logo_url]":{
            required:true,
            url: true
        },
        "data[UserClient][website]": {
            required:true,
            url: true
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#edit_client").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#edit_client_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success_client').html("<div class='alert alert-success'>");
             $('#success_client > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_client > .alert-success')
                 .append("<strong>Update was successful</strong>");
             $('#success_client > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#edit_client_save_btn").html('Save');
             location.reload();
           }

           else {
             // Fail message
             $('#success_client').html("<div class='alert alert-danger'>");
             $('#success_client > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_client > .alert-danger').append("<strong>Profile could not be updated. Try again.</strong>");
             $('#success_client > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#edit_client_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});


//Validate and add a new portfolio
$(function(){
  $('#portfolio_add').validate({
    rules: {
        "data[Portfolio][title]": {
            required:true,
            maxlength:15
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#portfolio_add").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#add_portfolio_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success_portfolio').html("<div class='alert alert-success'>");
             $('#success_portfolio > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_portfolio > .alert-success')
                 .append("<strong>Update was successful</strong>");
             $('#success_portfolio > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#add_portfolio_save_btn").html('Save');
             location.reload();
           }

           else if(data == 'already_exist') {
             // Fail message
             $('#success_portfolio').html("<div class='alert alert-danger'>");
             $('#success_portfolio > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_portfolio > .alert-danger').append("<strong>This portfolio already exists. Try another.</strong>");
             $('#success_portfolio > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#add_portfolio_save_btn").html('Save');
           }

           else {
             // Fail message
             $('#success_portfolio').html("<div class='alert alert-danger'>");
             $('#success_portfolio > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_portfolio > .alert-danger').append("<strong>Profile could not be updated. Try again.</strong>");
             $('#success_portfolio > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#add_portfolio_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});



//Validate and add a new social network
$(function(){
  $('#social_network_add').validate({
    rules: {
        "data[UserSocial][social_network_id]": {
            required:true
        },
        "data[UserSocial][title]":{
            required:true
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#social_network_add").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#add_social_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success_social_network').html("<div class='alert alert-success'>");
             $('#success_social_network > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_social_network > .alert-success')
                 .append("<strong>Social network was successfully added</strong>");
             $('#success_social_network > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#add_social_save_btn").html('Save');
             location.reload();
           }

           else if(data == 'already_exist') {
             // Fail message
             $('#success_social_network').html("<div class='alert alert-danger'>");
             $('#success_social_network > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_social_network > .alert-danger').append("<strong>You have previously added this social network. Please try another.</strong>");
             $('#success_social_network > .alert-danger').append('</div>');
             $("#add_social_save_btn").html('Save');
           }

           else {
             // Fail message
             $('#success_social_network').html("<div class='alert alert-danger'>");
             $('#success_social_network > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_social_network > .alert-danger').append("<strong>Social network could not be added. Please try again.</strong>");
             $('#success_social_network > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#add_social_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});


//Validate and add a new testimonial
$(function(){
  $('#testimonial_add').validate({
    rules: {
        "data[Testimonial][name]": {
            required:true
        },
        "data[Testimonial][person_details]": {
            required:true
        },
        "data[Testimonial][description]":{
            required:true
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     var formData = new FormData(form);

     $.ajax({
         url: $("#testimonial_add").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#add_testimonial_save_btn").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Saving...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success_testimonial').html("<div class='alert alert-success'>");
             $('#success_testimonial > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_testimonial > .alert-success')
                 .append("<strong>Testimonial was successfully added</strong>");
             $('#success_testimonial > .alert-success')
                 .append('</div>');

             //clear all fields
             $("#add_testimonial_save_btn").html('Save');
             location.reload();
           }

           else {
             // Fail message
             $('#success_testimonial').html("<div class='alert alert-danger'>");
             $('#success_testimonial > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success_testimonial > .alert-danger').append("<strong>Social network could not be added. Please try again.</strong>");
             $('#success_testimonial > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#add_testimonial_save_btn").html('Save');
           }
         },
     });
       return false;
   }
});
