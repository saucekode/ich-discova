$(function(){
  $('#loginform').validate({
    rules: {
        "username": {
            required:true
        },
        "password": {
            required:true
        }
    },
    messages: {
        "username": {
            "required":"Please enter your email address"
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#loginform").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#login_button").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Logging in...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success').html("<div class='alert alert-success'>");
             $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-success')
                 .append("<strong>Login was successful</strong>");
             $('#success > .alert-success')
                 .append('</div>');

             //clear all fields
             $('#loginform').trigger("reset");
             $("#login_button").html('Login');
             window.location.href = $("#loginform").attr("redirect");
           }

           else if (data == 'account_not_found') {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>This account does not exist</strong>");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#login_button").html('Login');
           }

           else if (data == 'invalid_username') {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Ussername/Password is incorrect.</strong>");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#login_button").html('Login');
           }

           else {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Please activate your account first</strong>");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#login_button").html('Login');
           }
         },
         error: function() {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Sorry, it seems that my server is not responding. Please try again later!</strong>");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#login_button").html('Login');
         },
     });
       return false;
   }
});
