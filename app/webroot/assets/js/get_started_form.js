$(function(){
  $('#getStartedForm').validate({
    rules: {
        "get_started_name": {
            required:true
        },
        "get_started_email": {
            required:true,
            email:true
        },
        "get_started_password": {
            required:true
        },
        "get_started_category_id": {
            required:true
        },
        "get_started_url": {
            required:true
        }
    },
    messages: {
        "get_started_name": {
            "required":"Please enter your full name"
        }
    },
    submitHandler: submitForm
  });

    function submitForm(form)
    {
     //var data = $("#ManuscriptOfflineSubmitForm").serialize();
     var formData = new FormData(form);

     $.ajax({
         url: $("#getStartedForm").attr("action"),
         type: "POST",
         data : formData,
         contentType: false,
         cache: false,
         processData: false,
         beforeSend: function()
         {
           $("#get_started_submit").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Creating account...');
         },
         success: function(data) {
           if(data == 'success'){
             // Success message
             $('#success').html("<div class='alert alert-success'>");
             $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-success')
                 .append("<strong>Your account was successfully created. Please check your mail to confirm your account.</strong>");
             $('#success > .alert-success')
                 .append('</div>');

             //clear all fields
             $('#getStartedForm').trigger("reset");
             $("#get_started_submit").html('Get Started');
           }

           else if (data == 'domain_exist') {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>This domain name already exists. Please try another.");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#get_started_submit").html('Get Started');
           }

           else if (data == 'already_exist') {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>This user already exists. Please try another email address.");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#get_started_submit").html('Get Started');
           }

           else {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Sorry, your data could not be saved. Please try again!");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#get_started_submit").html('Get Started');
           }
         },
         error: function() {
             // Fail message
             $('#success').html("<div class='alert alert-danger'>");
             $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                 .append("</button>");
             $('#success > .alert-danger').append("<strong>Sorry, it seems that my server is not responding. Please try again later!");
             $('#success > .alert-danger').append('</div>');
             //clear all fields
             //$('#getStartedForm').trigger("reset");
             $("#get_started_submit").html('Get Started');
         },
     });
       return false;
   }
});




/*$(function() {

    $("input,textarea,select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $("#get_started_name").val();
            var email = $("#get_started_email").val();
            var password = $("#get_started_password").val();
            var category_id = $("#get_started_category_id").val();
            var discover_url = $("#get_started_url").val();

            $.ajax({
                url: $("#getStartedForm").attr("action"),
                type: "POST",
                data: {
                    name: name,
                    email: email,
                    password: password,
                    discover_url: discover_url,
                    category_id: category_id
                },
                cache: false,
                beforeSend: function()
                {
                  $("#get_started_submit").html('<i class="fa fa-spinner fa-spin"></i> &nbsp; Creating account...');
                },
                success: function(data) {
                  if(data == 'success'){
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your account was successfully created. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#getStartedForm').trigger("reset");
                    $("#get_started_submit").html('Get Started');
                  }

                  else if (data == 'already_exist') {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>This user already exists. Please try another email address.");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    //$('#getStartedForm').trigger("reset");
                    $("#get_started_submit").html('Get Started');
                  }

                  else {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry, your data could not be saved. Please try again!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    //$('#getStartedForm').trigger("reset");
                    $("#get_started_submit").html('Get Started');
                  }

                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry, it seems that my server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    //$('#getStartedForm').trigger("reset");
                    $("#get_started_submit").html('Get Started');
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes
$('#get_started_name').focus(function() {
    $('#success').html('');
});*/
