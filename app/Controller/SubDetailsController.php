<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class SubDetailsController extends AppController {
  public $components = array('Paginator');

  public function get_similar_profiles($cat_id,$user_id){
    $this->layout = false;
    $users = $this->SubDetail->find('all',array(
      'conditions'=>array(
        'AND'=>array(
          'SubDetail.category_id'=>$cat_id,
          'SubDetail.user_id !='=>$user_id
        )
      ),
      'order'=>'RAND()',
      'limit'=>5
    ));
    return $users;
  }

  public function get_category_users($cat_name){
    $category = $this->SubDetail->Category->findByName($cat_name);
    if(empty($category)){
      $this->redirect('/');
    } else {
      $this->Paginator->settings = array(
        'conditions'=>array(
          'AND'=>array(
            'SubDetail.category_id'=>$category['Category']['id']
          )
        ),
        'order'=>'User.name ASC',
        'limit'=>15
      );
      $users = $this->Paginator->paginate('SubDetail');

      $this->set(compact('users'));
      $this->set(compact('category'));
    }
  }

  public function get_all_users(){

    $this->Paginator->settings = array(
      'order'=>'User.name ASC',
      'limit'=>15
    );
    $users = $this->Paginator->paginate('SubDetail');

    $this->set(compact('users'));
  }

  public function get_featured_users(){
    $this->layout = false;
    $this->SubDetail->Behaviors->attach('Containable');
    $this->SubDetail->contain(array('UserSocial'=>array('SocialNetwork'),'User'));

    $users = $this->SubDetail->find('all',array(
      'conditions'=>array(
        'AND'=>array(
          'User.status'=>1,
          'SubDetail.featured'=>1
        )
      ),
      'order'=>'RAND()',
      'limit'=>8
    ));
    return $users;
  }


  public function upload_profile_pix(){
    $this->layout = false;

    $user = $this->Session->read('Auth.User.id');

    if($this->request->is('post')){
      $this->SubDetail->Behaviors->attach('Containable');
      $this->SubDetail->contain(array());
      $sub_detail = $this->SubDetail->findByUserId($user);

      if(!empty($sub_detail['SubDetail']['profile_pic'])){
        $filename = $sub_detail['SubDetail']['profile_pic'];
      } else {
        $filename = "profile_".$_FILES['file-0']['name'];
      }

      $dest_file = WWW_ROOT . "img/profiles/" . $filename;
      if(move_uploaded_file($_FILES['file-0']['tmp_name'],$dest_file )){
        $this->SubDetail->id = $sub_detail['SubDetail']['id'];
        $this->SubDetail->saveField('profile_pic',$filename);
        echo 'success';
      } else {
        echo 'failed';
      }

    }
  }

}
?>
