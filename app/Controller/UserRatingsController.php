<?php
App::uses('AppController', 'Controller');
class UserRatingsController extends AppController {
  public function rate_user(){
    $this->layout = false;
    $rating_value = $_POST['rating_value'];
    $sub_detail_id = $_POST['sub_detail_id'];
    $sub_rating_id = $_POST['sub_rating_id'];
    $user_id = $this->Session->read('Auth.User.id');

    $get_rating = $this->UserRating->findBySubRatingIdAndUserId($sub_rating_id,$user_id);

    $get_sub_rating = $this->UserRating->SubRating->findBySubDetailId($sub_detail_id);

    $number_votes = $get_sub_rating['SubRating']['number_votes'];
    $total_points = $get_sub_rating['SubRating']['total_points'];
    $dec_avg = $get_sub_rating['SubRating']['dec_avg'];
    $whole_avg = $get_sub_rating['SubRating']['whole_avg'];

    if(!empty($get_rating)){
      $users_former_rating = $get_rating['UserRating']['rating'];
    } else {
      $users_former_rating = 0;
    }

    if(empty($get_rating)){
      $data = array(
        'sub_rating_id' => $sub_rating_id,
        'user_id' => $user_id,
        'rating' => $rating_value
      );
      $this->UserRating->save($data);

      $data_to_update = array(
        'number_votes' => $number_votes + 1,
        'total_points' => $total_points + $rating_value,
        'dec_avg' => ($total_points + $rating_value)/($number_votes+1),
        'whole_avg' => round(($total_points + $rating_value)/($number_votes+1))
      );

    } else {
      $this->UserRating->id = $get_rating['UserRating']['id'];
      $this->UserRating->saveField('rating',$rating_value);

      $data_to_update = array(
        'total_points' => ($total_points - $users_former_rating) + $rating_value,
        'dec_avg' => (($total_points - $users_former_rating) + $rating_value)/$number_votes,
        'whole_avg' => round((($total_points - $users_former_rating) + $rating_value)/$number_votes)
      );
    }


    $this->UserRating->SubRating->id = $sub_rating_id;
    if($this->UserRating->SubRating->save($data_to_update)){
      echo "success";
    } else {
      echo "failed";
    }

  }
}
?>
