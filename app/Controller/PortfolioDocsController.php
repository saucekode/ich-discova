<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class PortfolioDocsController extends AppController {

  public function upload_pix($file,$filename,$folder_name){
    $dest_file = WWW_ROOT . "img/project_pictures/".$folder_name."/" . $filename;

    move_uploaded_file($file,$dest_file );
  }

  function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
      foreach ($file_keys as $key) {
        $file_ary[$i][$key] = $file_post[$key][$i];
      }
    }

    return $file_ary;
  }

  public function ajax_add(){
    $this->layout = false;
    $portfolio_id = $this->request->data['PortfolioDoc']['portfolio_id'];
    if($this->request->is('post')){
      $title = $_POST['title'];
      $port_type = $this->request->data['PortfolioDoc']['portfolio_type_id'];
      $data = array(
        'title' => $title
      );
      $check_port_name = $this->PortfolioDoc->PortfolioType->findById($port_type);

      //Check for type project
      if($check_port_name['PortfolioType']['title'] == 'Project'){
        $data['description'] = $_POST['description'];
        $pictures = array();

        $dir_path = WWW_ROOT . 'img/project_pictures/';
        $new_dir = $dir_path.$portfolio_id."/";
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777);
        }

        $file_ary = $this->reArrayFiles($_FILES['pictures']);
        foreach ($file_ary as $file) {
          $pix_name = 'project'.time().'_'.$file['name'];
          $this->upload_pix($file['tmp_name'],$pix_name,$portfolio_id);

          //Pass the filename to the array
          $pictures[] = $pix_name;
        }

        $data['pictures'] = $pictures;
        $data['client_name'] = $_POST['client_name'];

        $enc_data = json_encode($data);

      }
      //Check for type Music
      else if($check_port_name['PortfolioType']['title'] == 'Music'){
        $data['description'] = $_POST['description'];
        $data['url'] = $_POST['url'];
        $enc_data = json_encode($data);

      } else {
        $data['description'] = $_POST['description'];
        $data['url'] = $_POST['url'];
        $enc_data = json_encode($data);
      }



      $save_data = array(
        'title' => $title,
        'form_details' => $enc_data,
        'portfolio_id' => $portfolio_id,
        'portfolio_type_id' => $port_type
      );
      $submit_data = $this->PortfolioDoc->save($save_data);

      if($submit_data){
        echo "success";
      } else {
        echo "failed";
      }
    }
  }
}
?>
