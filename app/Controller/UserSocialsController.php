<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class UserSocialsController extends AppController {
  public function ajax_add(){
    $this->layout = false;
    if ($this->request->is('post')) {
      $social_id = $this->request->data['UserSocial']['social_network_id'];
      $sub_detail_id = $this->request->data['SubDetail']['id'];
      $find_social_id = $this->UserSocial->findBySocialNetworkIdAndSubDetailId($social_id,$sub_detail_id);

      if(empty($find_social_id)){
        $this->request->data['UserSocial']['sub_detail_id'] = $sub_detail_id;
        $this->UserSocial->create();
        if($this->UserSocial->save($this->request->data)){
          echo "success";
        } else {
          echo "failed";
        }
      } else {
        echo "already_exist";
      }

    }
  }
}
?>
