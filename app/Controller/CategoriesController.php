<?php
App::uses('AppController', 'Controller');
class CategoriesController extends AppController {
  public function get_categories(){
    $cats = $this->Category->find('list');
    return $cats;
  }

  public function get_cat_menu(){
    $cats = $this->Category->find('all',array('fields'=>array('Category.id','Category.name')));
    return $cats;
  }
}
?>
