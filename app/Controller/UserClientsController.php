<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class UserClientsController extends AppController {
  public function ajax_add_client(){
    $this->layout = false;
    if ($this->request->is('post')) {
      $this->request->data['UserClient']['sub_detail_id'] = $this->request->data['SubDetail']['id'];
      $this->UserClient->create();
      if($this->UserClient->save($this->request->data)){
        echo "success";
      } else {
        echo "failed";
      }
    }
  }

  public function ajax_edit_client($id){
    $this->layout = false;
    $client = $this->UserClient->findById($id);
    $this->set(compact('client'));

    if(!empty($client)){
      $this->request->data = $client;
    }
  }

  public function submit_ajax_edit_client(){
    $this->layout = false;
    if($this->request->is('post')){
      $this->UserClient->id = $this->request->data['UserClient']['id'];
      if($this->UserClient->save($this->request->data)){
        echo "success";
      } else {
        echo "failed";
      }
    }
  }

  public function ajax_client_delete(){
    $this->layout = false;
    $id = $_POST['del_id'];
    $this->UserClient->delete($id);
  }
}
?>
