<?php
App::uses('AppController', 'Controller');
class ConfigsController extends AppController {
  public function get_trial_days(){
    $days = $this->Config->findByShortName('trial_days');
    return $days['Config']['details'];
  }
}
?>
