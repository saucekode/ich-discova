<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class PortfolioTypesController extends AppController {
  public function get_form_details(){
    $this->layout = false;
    $type_id = $_GET['type_id'];
    $type = $this->PortfolioType->findById($type_id);
    echo $type['PortfolioType']['form_data'];
  }

  public function get_title($id){
    $this->layout = false;
    $port_title = $this->PortfolioType->findById($id);
    return $port_title['PortfolioType']['title'];
  }
}
?>
