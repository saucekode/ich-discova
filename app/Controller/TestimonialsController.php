<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class TestimonialsController extends AppController {
  public function ajax_add(){
    $this->layout = false;
    if($this->request->is('post')){
      $sub_detail_id = $this->request->data['SubDetail']['id'];
      $this->request->data['Testimonial']['sub_detail_id'] = $sub_detail_id;
      $this->Testimonial->create();
      if($this->Testimonial->save($this->request->data)){
        echo "success";
      } else {
        echo "failed";
      }
    }
  }
}
?>
