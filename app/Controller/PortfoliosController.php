<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class PortfoliosController extends AppController {
  public function ajax_add(){
    $this->layout = false;
    if($this->request->is('post')){
      $sub_id = $this->request->data['SubDetail']['id'];
      $title = $this->request->data['Portfolio']['title'];
      $find_title = $this->Portfolio->findBySubDetailIdAndTitle($sub_id, $title);

      if(empty($find_title)){
        $this->request->data['Portfolio']['sub_detail_id'] = $sub_id;
        $this->Portfolio->create();
        if($this->Portfolio->save($this->request->data)){
          echo "success";
        } else {
          echo "failed";
        }
      } else {
        echo "already_exist";
      }

    }
  }

  public function add_items($id){
    $this->layout = false;
    $portfolio = $this->Portfolio->findById($id);
    $this->set(compact('portfolio'));

    $portfolio_types = $this->Portfolio->PortfolioDoc->PortfolioType->find('list');
    $this->set(compact('portfolio_types'));
  }
}
?>
