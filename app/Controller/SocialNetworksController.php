<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
class SocialNetworksController extends AppController {
  public function getSocialNetworksList(){
    $this->layout = false;
    $networks = $this->SocialNetwork->find('list');
    return $networks;
  }

  public function getSocialFontCode(){
    $this->layout = false;
    $id = $_GET['type_id'];
    $network = $this->SocialNetwork->findById($id);
    echo $network['SocialNetwork']['font_code'];
  }

  public function getSocialBaseUrl(){
    $this->layout = false;
    $id = $_GET['type_id'];
    $network = $this->SocialNetwork->findById($id);
    echo $network['SocialNetwork']['base_url'];
  }
}
?>
