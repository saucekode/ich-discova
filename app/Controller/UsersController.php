<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Configs');
App::uses('Security', 'Utility');
class UsersController extends AppController {

  public $components = array(
      'Email',
      'Paginator',
      'Session',
      'Auth' => array(
          'authenticate' => array(
              'Form' => array(
                  'passwordHasher' => 'Blowfish'
              )
          )

      ),
  );

  public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('__sendActivationEmail','__generateActivationToken','checklogin','activate_account','login','ajax_login','ajax_sign_up','logout','get_user_domain','get_user_domain_two');
  }

  public function checklogin(){
      if($this->Session->check('Auth.User')){
          return true;
      }
      return false;
  }

  function __generateActivationToken(){
      $token = "";
      for ($i = 0; $i < 100; $i++) {
          $d = rand(1, 100000) % 2;
          $d ? $token .= chr(rand(33,79)) : $token .= chr(rand(80,126));
      }

      (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;

      // Generate hash of random string
      $hash = Security::hash($token, 'sha256', true);;
      for ($i = 0; $i < 20; $i++) {
          $hash = Security::hash($hash, 'sha256', true);
      }

      return $hash;
  }

  function __sendActivationEmail($id = null) {
      if (!empty($id)) {
        $this->User->id = $id;
        $user = $this->User->read();

        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('default');

        $email->from(array('info@discova.com.ng'=>'Discova.ng'));
        $email->to($user['User']['email']);
        $email->subject('Welcome to Discova.com.ng');
        $email->template('account_activation', null);
        $email->viewVars(compact('user'));
        $email->emailFormat('html');
        $email->send();

        return true;
      }
      return false;
  }

  public function activate_account($confirmation_code){
    $this->render = false;
    if(!($confirmation_code)){
      $this->redirect('/');
    }
    $user = $this->User->findByConfirmationCode($confirmation_code);
    if(empty($user)){
      $this->Session->setFlash('This account could not be confirmed','flash_error');
      $this->redirect('/');
    }
    $this->User->id = $user['User']['id'];
    $this->User->saveField('status',1);
    $this->Session->setFlash('Your account was successfully confirmed','flash_success');
    $this->redirect('/login');
  }

  public function getCurrentUser(){
    $user = $this->Session->read('Auth.User');
    if($user){
      return $user;
    }
    return false;
  }

  public function dashboard(){
    $user_id = $this->getCurrentUser();
    $this->User->Behaviors->attach('Containable');
    $this->User->contain(array('SubDetail'=>array('UserSocial'=>array('SocialNetwork'),'UserClient','UserService','Testimonial','Portfolio'=>array('PortfolioDoc'=>array('PortfolioType')))));
    $user = $this->User->find('all',array('conditions'=>array('User.id'=>$user_id['id'])));
    $this->set(compact('user'));

    if(!empty($user)){
      $this->request->data = $user[0];
    }
  }

  public function get_user_domain($domain_name){
    $this->layout = 'user_domain';
    $this->User->Behaviors->attach('Containable');
    $this->User->contain(array('SubDetail'=>array('UserSocial'=>array('SocialNetwork'),'UserClient','UserService','Testimonial','Portfolio'=>array('PortfolioDoc'=>array('PortfolioType')))));
    $user = $this->User->find('all',array('conditions'=>array('SubDetail.discover_url'=>$domain_name)));
    $this->set(compact('user'));

  }

  public function get_user_domain_two($domain_name){
    $this->User->Behaviors->attach('Containable');
    $this->User->contain(array('SubDetail'=>array('UserSocial'=>array('SocialNetwork'),'UserClient','UserService','Testimonial','Portfolio'=>array('PortfolioDoc'=>array('PortfolioType')),'SubRating')));

    $user = $this->User->find('all',array('conditions'=>array('SubDetail.discover_url'=>$domain_name)));

    $todays_date = date('Y-m-d h:i:s');
    if(($todays_date > $user[0]['SubDetail']['sub_expiry_date']) || ($user[0]['SubDetail']['active'] == 0)){
      $this->User->SubDetail->id = $user[0]['SubDetail']['id'];
      $this->User->SubDetail->saveField('active',0);
      $this->redirect('/profile-not-found');
    }
    $this->set(compact('user'));

  }

  public function ajax_update_profile(){
    $this->layout = false;
    if ($this->request->is('post')) {
      $this->User->id = $this->request->data['User']['id'];
      if($this->User->save($this->request->data)){
        echo "success";
      } else {
        echo "failed";
      }
    }
  }

  public function login(){
    if($this->checklogin()){
        $this->redirect(array('action' => 'dashboard'));
    }
  }

  public function logout() {
      $this->Auth->logout();
      $this->redirect('/login');
  }

  public function ajax_login(){
    $this->layout = false;

    if ($this->request->is('post')) {
      $username = $_POST['username'];
      $password = $_POST['password'];

      $this->request->data['User']['username'] = $username;
      $this->request->data['User']['password'] = $password;
      $user = $this->User->findByUsername($username);
      if (empty($user)) {
        echo "account_not_found";
      } elseif ($user['User']['status'] == 1) {
          if ($this->Auth->login()) {
            echo "success";
          } else {
            $resp = array();
            echo "invalid_username";
          }
      } else {
        echo "not_active";
      }
    }
  }

  public function ajax_sign_up(){
    $this->layout = false;
    if($this->request->is('post')){
      $cat_id = $_POST['get_started_category_id'];
      $dis_url = $_POST['get_started_url'];

      $data = array(
        'name'=>$_POST['get_started_name'],
        'username'=>$_POST['get_started_email'],
        'email'=>$_POST['get_started_email'],
        'password'=>$_POST['get_started_password'],
        'status'=>0,
        'confirmation_code'=>$this->__generateActivationToken()
      );
      $check_user = $this->User->findByEmail($_POST['get_started_email']);
      if(empty($check_user)){
        $find_url = $this->User->SubDetail->findByDiscoverUrl($dis_url);
        if(empty($find_url)){
          $this->User->create();
          if($this->User->save($data)){

            $last_id = $this->User->id;

            $ConfigsController = new ConfigsController;
            $sub_trial_days = $ConfigsController->get_trial_days();

            $data = array(
              'discover_url'=>$dis_url,
              'category_id'=>$cat_id,
              'user_id'=>$last_id,
              'sub_date'=>date('Y-m-d h:i:s'),
              'last_sub_date'=>date('Y-m-d h:i:s'),
              'sub_expiry_date'=> date('Y-m-d h:i:s', strtotime("+$sub_trial_days days")),
              'sub_means'=>'web',
              'active'=>1
            );
            //$this->User->SubDetail->create();
            $this->User->SubDetail->saveAssociated($data);

            $last_sub_detail_id = $this->User->SubDetail->getLastInsertId();
            $sub_data = array(
              'number_votes'=>0,
              'total_points'=>0,
              'dec_avg'=>0,
              'whole_avg'=>0,
              'sub_detail_id'=>$last_sub_detail_id
            );
            $this->User->SubDetail->SubRating->save($sub_data);

            $this->__sendActivationEmail($last_id);

            echo "success";

        } else {
          echo "failed";
        }
      } else {
        echo "domain_exist";
      }
    } else {
      echo "already_exist";
    }

    }
  }

}
 ?>
